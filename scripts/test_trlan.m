clear

% > matrix dimension
n = 1000;
n = 30;
% > type of test matrix, diag((1:n)^k)
k = 2;
% > diagonal test matrix        
d = 1:n;
d = d.^k;
A = sparse(diag(d));
Anrm = n^k;

% > solver choice
% 1 for trlan
% 2 for trlan_svd_ne (normal eq based SVD)
% 3 for trlan_svd_bt (butter-fly based SVD)
solver = 1;
% > target eigenvalues
% 1 for big ones, -1 for small ones, 0 for both side
lohi=-1;

% > solver parameters
ned = 5;                       % # of eigenvalues to be computed at time
tol = 1e-8/Anrm;               % relative residual norm tol

% > TRLan-EED parameters
restart  = max(30,2*ned);      % restart cycle
maxiters = min(n, 5*restart);  % max # of SpMV
alpha = floor(n/2)^k;          % shift for EED
h = 2;                         % how many time to call TRLan, 
                               %  i.e., h*ned is total number of computed eigenvalues

fp = fopen('trlan_out.dat','w');
fprintf(1,  ' > Anrm = %.2e\n',Anrm);
fprintf(fp, ' > Anrm = %.2e\n',Anrm);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Call TRLan to compute h*ned from beginning
fprintf(1,  ' > trlan(lohi=%d, ned=%d, tol=%.1e, maxiters=%d, restart=%d\n',lohi,h*ned,tol,maxiters,h*restart);
fprintf(fp, ' > trlan(lohi=%d, ned=%d, tol=%.1e, maxiters=%d, restart=%d\n',lohi,h*ned,tol,maxiters,h*restart);
v0 = rand(n,1);
tic;
[s,v,count,iters] = trlan(A, v0, lohi, h*ned, tol, h*maxiters, h*restart);
end_time = toc;

fprintf(1,  ' > took %.2e seconds\n',end_time);
fprintf(fp, ' > took %.2e seconds\n',end_time);

% residual vectors
r = A*v - v*diag(s);

fprintf(1,  ' > resnorm = %.2e (count=%d, iters=%d)\n', norm(r),count,iters);
fprintf(fp, ' > resnorm = %.2e (count=%d, iters=%d)\n', norm(r),count,iters);        
for i=1:(h*ned)
    fprintf(1,  ' %4d: %.2e (err=%.2e, res=%.2e)\n', i,s(i),abs(d(i)-s(i)),norm(r(:,i)));
    fprintf(fp, ' %4d: %.2e (err=%.2e, res=%.2e)\n', i,s(i),abs(d(i)-s(i)),norm(r(:,i)));
end
fprintf(1,  '\n\n');
fprintf(fp, '\n\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Call TRLan-EED to compute ned at a time
V = [];
S = [];
tot_time = 0;
for j=1:h
    % > call TRLan
    fprintf(1,  ' > trlan-eed(lohi=%d, ned=%d, tol=%.1e, maxiters=%d, restart=%d\n',lohi,ned,tol,maxiters,restart);
    fprintf(fp, ' > trlan-eed(lohi=%d, ned=%d, tol=%.1e, maxiters=%d, restart=%d\n',lohi,ned,tol,maxiters,restart);
    v0 = ones(n,1); %rand(n,1);
    tic;
    [s,v,count,iters] = trlan_eed(A, alpha, V, v0, lohi, ned, tol, maxiters, restart);
    end_time = toc;
    tot_time = tot_time+end_time;
    fprintf(1,  ' > took %.2e seconds -> total %.2e seconds\n',end_time,tot_time);
    fprintf(fp, ' > took %.2e seconds -> total %.2e seconds\n',end_time,tot_time);
    
    % residual vectors
    V = [V, v(:,1:ned)];
    S = [S; s(1:ned)];
    r = A*V - V*diag(S);

    % > check residual norms
    if lohi > 0
        d = d(n:-1:1);
    end
    fprintf(1,  ' > resnorm = %.2e (count=%d, iters=%d)\n', norm(r),count,iters);
    fprintf(fp, ' > resnorm = %.2e (count=%d, iters=%d)\n', norm(r),count,iters);        
    for i=1:(j*ned)
        fprintf(1,  ' %4d: %.2e (err=%.2e, res=%.2e)\n', i,S(i),abs(d(i)-S(i)),norm(r(:,i)));
        fprintf(fp, ' %4d: %.2e (err=%.2e, res=%.2e)\n', i,S(i),abs(d(i)-S(i)),norm(r(:,i)));
    end
    fprintf(1,  '\n' );
    fprintf(fp, '\n' );
end
fclose(fp);
