function [e, v, count, total_iters] = trlan_eed(A, alpha, U, v0, lohi, ned, tol, maxiters, restart)
% trlan_eed: thick-restart Lanczos with explicit external deflation 
%            to compute singular values of A+alpha*U*U'
%     [S, V, COUNT, TOTAL_ITERS] = TRLAN_EED(A, alpha, U, LOHI, NED, TOL, MAXITERS, RESTART)
%     Inputs:
%     A, alpha, U : coefficient matrix, A + alpha*U*U'
%     V0          : starting vector
%     LOHI        : 1 to compute largest singular values, or
%                  -1 to compute smallest singular values (not yet)
%     NED         : number of eigenvalues to be computed
%     TOL         : relative residual norm tolerance
%     MAX_ITERS   : maximum number of matrix operations
%     RESTART     : restart cycle
%     Outputs:
%     E           : eigenvalues
%     V           : eigenvectors
%     COUNT       : number of restart loops performed
%     TOTAL_ITERS : number of matrix operations performed
%
% email iyamazak@icl.utk.ed to report bugs or questions.
%
%
    % call TRLan with A+alpha*U*U'
    [e, v, count, total_iters] = trlan(@spmv, v0, lohi, ned, tol, maxiters, restart);
    
    % op function
    function y = spmv(x)  
        y = A*x;
        if size(U,2) > 0
            y = y + alpha*(U*(U'*x));
        end
    end
end