function [s, v, count, total_iters] = trlan(A, v0, lohi, ned, tol, max_iters, restart)
% trlan: thick-restart Lanczos to compute eigenvalues of A.
%     [S, V, COUNT, TOTAL_ITERS] = TRLAN(A, LOHI, NED, TOL, MAXITERS, RESTART)
%     Inputs:
%     LOHI        : 1 to compute largest eigenvalues, 
%                  -1 to compute smallest eigenvalues, or
%                   0 to compute largest singular values
%     NED         : number of eigenvalues to be computed
%     TOL         : relative residual norm tolerance
%     MAX_ITERS   : maximum number of matrix operations
%     RESTART     : restart cycle
%     Outputs:
%     S           : eigenvalues
%     V           : eigenvectors
%     COUNT       : number of restart loops performed
%     TOTAL_ITERS : number of matrix operations performed
%
% email iyamazak@icl.utk.edu to report bugs or questions.
%
%
if restart <= ned+3
    fprintf(1, ' restart-cycle is too small (ned=%d, restart=%d)\n',ned,restart);
    return;
end

% initialize parameters
n = size(v0,1);
Afunc = isa(A, 'function_handle');
total_iters = 0;
converged = false;
nkept = 0;
count = 1;

reortho = true;
verbose = 0;

% allocate 
Anrm    = n;
q       = zeros(n,restart+1);
alpha   = zeros(restart,1);
beta    = zeros(restart,1);
resvecs = zeros(restart,1);
T = zeros(restart+1,restart);

% initialize starting vector
rng('default');
q(:,1) = v0;
q(:,1) = q(:,1) / norm(q(:,1));

% == thick-restarted Lanczos == %
while converged == false && total_iters < max_iters 
    if verbose > 0
        fprintf(1, '\n >> restart-loop=%d (iter=%d) <<\n',count,total_iters);
    end
    
    % == main Lanczos iteration == %
    for j = (nkept+1):restart
        % > SpMV <
        if Afunc
            w = A( q(:,j) );
        else
            w = A*q(:,j);
        end
        % > ortho against q(:,j) <
        alpha(j) = w'*q(:,j);
        w = w - alpha(j)*q(:,j);
        % > ortho against q(:,j-1) <
        if (j > 1)
            if j == nkept+1
                % > first iter needs full ortho
                w = w - q(:,1:nkept)*beta(1:nkept);
            else
                w = w - q(:,j-1)*beta(j-1);
            end
        end
        % > reortho <
        if reortho
            w = w - q(:,1:j)*(q(:,1:j)'*w);
        end
        % > vector norm <
        beta(j) = norm(w);
        if verbose > 0 
            fprintf(1, ' alpha(%d) = %.2e, beta(%d) = %.2e\n',j,alpha(j),j,beta(j));
        end
        % > normalize <
        if beta(j) < Anrm*eps
            % seems to hit the invaliant subspace
            q(:,j+1) = rand(n,1);
            q(:,j+1) = q(:,j+1) - q(:,1:j)*(q(:,1:j)'*q(:,j+1));
            q(:,j+1) = q(:,j+1) / norm(q(:,j+1));
            if verbose > 0 
                fprintf(1, ' .. invaliant ..\n');
            end            
            beta(j) = 0.0;
        else
            q(:,j+1) = w / beta(j);
        end
        % > done <
        total_iters = total_iters + 1;
    end

    % == restart == %    
    if verbose > 0
        fprintf(1, ' >> restart <<\n');
    end
    % > solve projected system < %
    if lohi == 0
        % generate bi-diagonal
        B = zeros(restart/2, restart/2);
        if nkept > 0
            % * arrow-head part            
            B(1:nkept/2, 1:nkept/2) = diag(alpha(1:2:nkept));
            B(1:nkept/2, nkept/2+1) = beta(2:2:nkept);
        end
        % * tri-diagonal part
        B(nkept/2+1:restart/2, nkept/2+1:restart/2) ...
            = diag(beta(nkept+1:2:restart-1)) ...
              + diag(beta(nkept+2:2:restart-1),1);
        [y,t,z] = svd(B);
        % expand to trlan form
        x = zeros(restart,restart);
        x(1:2:restart, 1:2:restart) = z;
        x(2:2:restart, 2:2:restart) = y;
        e(1:2:restart) = diag(t);
        e(2:2:restart) = -diag(t);
    else
        % generate tri-diagonal
        T(nkept+1:restart, nkept+1:restart) ...
            = diag(alpha(nkept+1:restart)) ...
                + diag(beta(nkept+1:restart-1),1) ...
                + diag(beta(nkept+1:restart-1),-1);
        if verbose > 1    
            T(restart+1, restart) = beta(restart);
            fprintf(1, '\n norm(QQ-I) = %.2e\n',norm(q'*q-eye(restart+1,restart+1)));
            fprintf(1, ' norm(AQ - QT)=%.2e\n\n',norm(A*q(:,1:restart) - q(:,1:restart+1)*T));
        end        
        [x,e] = eig(T(1:restart,1:restart));
        e = diag(e);
    end
    if count == 1 || Anrm < e(restart)
        Anrm = abs(e(restart));
    end
    % * shuffle ritz pairs * %            
    if lohi > 0
        x = x(:,restart:-1:1);
        e = e(restart:-1:1);
    end
    % > project back < %
    v = q(:,1:restart)*x;
    s = e;
    
    % > compute residual norms (explicit for now) < %
    if Afunc
        if lohi == 0
            r1 = A( v(:,1:2:restart-1) ) - v(:,2:2:restart)*diag(e(1:2:restart));
            r2 = A( v(:,2:2:restart) ) - v(:,1:2:restart-1)*diag(e(1:2:restart));            
            r = zeros(n,restart);
            r(:,1:2:restart) = r1;
            r(:,2:2:restart) = r2;
        else
            r = A( v ) - v*diag(e);
        end
    else
        r = A*v - v*diag(e);
    end
    ncv = 0;    
    for i = 1:restart
        resvecs(i) = norm(r(:,i));
        if resvecs(i) < Anrm*tol
            ncv = ncv + 1;
        end
        if verbose > 0 && i <= ned
            res = beta(restart)*x(restart,i);
            fprintf(1, ' %5d: e=%.2e, resvecs = %.2e (%.2e), ncv=%d\n',i, e(i), ...
                         resvecs(i), res,ncv);
        end
    end
    if ncv >= ned || total_iters >= max_iters
        % all converged
        converged = true;
    end
    
    % == prepare to thick restart == %
    if converged == false
        count = count + 1;
        nkept = ned;
        if lohi ~= 0
            % one extra
            nkept = nkept+1;
            % let's also keep one big/small one
            nkept = nkept + 1;
            e(nkept) = e(restart);        
            x(:,nkept) = x(:,restart);
            v(:,nkept) = v(:,restart);
        end
        alpha(1:nkept) = e(1:nkept);
        beta(1:nkept) = beta(restart)*x(restart,1:nkept);            
        % > generate projected matrix T < %
        T(1:restart+1, 1:restart) = 0;        
        if lohi == 0
            % > generate 'tridiagonal' matrix < %
            e2 = zeros(nkept-1,1);
            e2(1:2:nkept-1) = e(1:2:nkept);
            T(1:nkept, 1:nkept) = diag(e2,1)+diag(e2,-1);
        else
            % > generate arrow-head matrix < %    
            T(1:nkept, 1:nkept) = diag(e(1:nkept));
        end
        T(1:nkept, nkept+1) = beta(1:nkept);
        T(nkept+1, 1:nkept) = beta(1:nkept);        
        % > kept ritz vectors + last Lanczos vector (i.e., residual vector) < %    
        q(:,1:nkept) = v(:,1:nkept);
        q(:,nkept+1) = q(:,restart+1);
        if verbose > 1
            fprintf(1, ' nkept = %d, restart = %d\n',nkept,restart);
            if Afunc
                fprintf(1, ' norm(AQ - QT)=%.2e\n',norm(A( q(:,1:nkept) ) - q(:,1:nkept+1)*T(1:nkept+1,1:nkept)));                
            else
                fprintf(1, ' norm(AQ - QT)=%.2e\n',norm(A*q(:,1:nkept) - q(:,1:nkept+1)*T(1:nkept+1,1:nkept)));
            end
        end
    end
end

if converged == false
    count = -1;
end
