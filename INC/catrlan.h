
void catrl_order_shifts( int m, int s, double *shift );
void catrl_matrix_kernel( int nrow, int mks, double *W, trl_matvec op, double *shift, void *mvparam );
void catrl_cgs( int nrow, int mV, double *V, int mW, double *W, int ldw, double *R, double *lR, trl_info *info );
void catrl_qr( int nrow, int m, double *W, double *lR, double *R, double *T, trl_info *info );
void catrl_update_proj( int ncol, double *R, double *shift, double *alpha, double *beta );
void catrl_update_proj2( int ncol, double *R, double *shift, double *alpha, double *beta );
