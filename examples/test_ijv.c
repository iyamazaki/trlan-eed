
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <float.h>
#include <time.h>
#include <math.h>

#include "trl_map.h"
#include "trlan.h"
////
// a simple matrix-vector multiplications routine
// defines a diagonal matrix with values (1, 4, 9, 16, 25, ....)
//
typedef struct {
  int    n, frow, nloc, nnz;
  int    *rowptr;
  int    *colind;
  double *nzval;
} csr;

int read_ijv  (FILE *fp, int *n, int *m, int *nnz, int **rowptr, int **colind, double **nzval );
int expand_ijv(FILE *fp, int *n, int *m, int *nnz, int **rowptr, int **colind, double **nzval );

void ijv_op(const int nrow, const int ncol, const double *xin, const int ldx,
            double *yout, const int ldy, void *mvparam) {
    //
    // ..
    // .. local variables ..
    int i, j, k;
    csr *mycsr;
    //
    // ..
    // .. executable statements ..
    mycsr = (csr*)mvparam;
    for( j=0; j<ncol; j++ )
	for( i=0; i<mycsr->n; i++ ) {
	  yout[j*nrow+i] = 0.0;
	  for( k=mycsr->rowptr[i]; k<mycsr->rowptr[i+1]; k++ )
	    yout[j*nrow+i] += mycsr->nzval[k] * xin[j*nrow+mycsr->colind[k]];
	}
}

//// a really simple example of how to use TRLAN
int main( int argn, char **argv ) {
    // This set of parameters tell TRLAN to compute 5 (NED) smallest
    //(LOHI=-1) of a 100 x 100 (NROW) matrix by using a Krylov subspace
    // basis size of 30 (MAXLAN).
    // MEV defines the size of the arrays used to store computed
    // solutions, eval, evec.
    int mev;
    // local variable declaration
    int n, m, nrow, nnz, lohi, ned, maxlan, restart, lwrk, lwrk2,
        mlan[3], rest[3];
    double *eval, *evec;
    double *res, *wrk;
    char buf[300];
    trl_info info;
    int i, j, k, check, nbas, ii, nmis, ifact, rfact;
    char name2[133], name[150], file[150];
    int tmp1, tmp2, nlen;
    clock_t t1, t2;
    FILE *ferr, *fp, *fin;
    csr  *mycsr;
    // initialize info -- tell TRLAN to compute NEV smallest eigenvalues
    // of the diag_op

    mycsr = malloc(sizeof(csr));

    fin = fopen( argv[1],"r" );
    //read_ijv(fin, &n, &m, &nnz, &(mycsr->rowptr), &(mycsr->colind), &(mycsr->nzval) );
    expand_ijv(fin, &n, &m, &nnz, &(mycsr->rowptr), &(mycsr->colind), &(mycsr->nzval) );
    mycsr->n   = n;
    mycsr->nnz = nnz;
    nrow = n;  printf( "nrow=%d\n",nrow );

    i = 0;
    fp = fopen( "input","r" );
    while( fgets(buf, 250, fp) != NULL ) {
      switch( i ) {
      case 0:
        sscanf(buf, "%d", &lohi);
        break;
      case 1:
        sscanf(buf, "%d", &ned );
        break;
      case 2:
        sscanf(buf, "%d %d %d", &(mlan[0]), &(mlan[1]), &(mlan[2]) );
        break;
      case 3:
        sscanf(buf, "%d %d %d", &(rest[0]), &(rest[1]), &(rest[2]) );
        break;
      }
      i++;
    }
    fclose(fp);

    mev = ned;
    eval = malloc( mev     *sizeof(double) );
    evec = malloc( mev*nrow*sizeof(double) );
    for( maxlan=mlan[0]; maxlan<=mlan[1]; maxlan+=mlan[2] ) {
	lwrk=1000*1100 + (nrow+4)*(maxlan-mev+1); 
	if( lwrk > 0 ) {
	    res = (double*)malloc(lwrk*sizeof(double));
	    wrk = (double*)malloc(lwrk*sizeof(double));
	}
	for( restart=rest[0]; restart<=rest[1]; restart+=rest[2] ) {
	    //trl_init_info( &info, nrow, maxlan, lohi, ned,
	    //	       1.4901e-8, restart, 500000, -1 );
            //trl_init_info( &info, nrow, maxlan, lohi, ned,
            //               1.0e-13, restart, 500000, -1 );
            trl_init_info( &info, nrow, maxlan, lohi, ned,
                           2.2204e-16, restart, 500000, -1 );

	    trl_set_iguess( &info, 0, 1, 0, NULL );
            info.mvparam = mycsr;

	    ferr=fopen( "error.txt","a" );
	    fprintf( ferr, " \n ~~ iteration (ned=%d, maxlan=%d, "
		     "restart=%d lohi=%d) ~~\n", ned, maxlan,
		     restart,lohi );
	    printf( " \r\n ~~ iteration (ned=%d, maxlan=%d, "
		    "restart=%d lohi=%d) ~~\r\n", ned, maxlan,
		    restart,lohi );
	    fclose(ferr);

	    // start with [1,1,...,1]^T
	    memset( eval, 0, mev*sizeof(double) );
	    for( i=0; i<nrow; i++ ) 
		evec[i] = 1.0;
	    if( sprintf( file,
			 "LOG_Nrow%dNed%dLan%dRes%dLoHi%d",
			 nrow,ned,maxlan,restart,lohi ) < 0 )  {
		printf( "error writing file name\r\n" );
	    } else {
		trl_set_debug( &info, 0, file );
		// call TRLAN to compute the eigenvalues
		t1 = clock();
		trlan(ijv_op, &info, nrow, mev, eval, evec, nrow,
		      lwrk, res );
		t2 = clock();
		printf( "TRLan: %d secs\n",
		        (int)((t2-t1)/CLOCKS_PER_SEC) );
		trl_print_info(&info, 3*nrow );
		i = info.nec;
		if( i > mev ) i = mev;
		lwrk2 = min( lwrk,i+nrow );
		lwrk2 = 4*i;
		if( info.nec == 0 ) info.nec = min(info.ned, mev-1);
		trl_check_ritz( ijv_op, &info, nrow, i, evec,
				nrow, eval, &check, res, NULL,
				wrk, lwrk2 );
		ferr = fopen( "error.txt","a" );
		if( info.stat != 0 ) {
		    fprintf( ferr,
			     " ** TRLAN FAILED WITH %d **\n **",
			     info.stat );
		}
		if( check < 0 ) {
		    fprintf( ferr,
			     " -------- ERROR (%d converged) --------\n ",
			     info.nec );
		    printf( " -------- ERROR (%d converged) --------\r\n ",
			    info.nec );
		} else if( info.nec < info.ned ) {
		    fprintf( ferr,
			     " ~~~~~~~~~ only %d converged ~~~~~~~~\n",
			     info.nec );
		    printf( " ~~~~~~~~~ only %d converged ~~~~~~~~\r\n",
			    info.nec );
		} else {
		    printf( " ********* SUCCESS (%d converged) **********\r\n",
			    info.nec );
		    fprintf( ferr,
			     " ********* SUCCESS (%d converged) **********\n",
			     info.nec );
		}
		fclose(ferr);
	    }
	}
	if( lwrk > 0 ) {
	    free(res);
	    free(wrk);
	}
    }
    free(mycsr->rowptr);
    free(mycsr->colind);
    free(mycsr->nzval);
    free(mycsr);
    free(eval);
    free(evec);
}
