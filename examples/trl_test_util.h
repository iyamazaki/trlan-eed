
#ifndef TRL_TEST_UTIL
#define TRL_TEST_UTIL

int read_dense( FILE *fp, double **exact );
int read_ijv(FILE *fp, int *n, int *m, int *nnz, int **rowptr, int **colind, double **nzval );
int expand_ijv(FILE *fp, int *n, int *m, int *nnz, int **rowptr, int **colind, double **nzval );

#endif
