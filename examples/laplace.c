
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "trl_test_putil.h"

int compareDouble(const void *a, const void *b) {
  double *aa = (double*) a;
  double *bb = (double*) b;
  if (*aa < *bb) {
    return -1;
  } else if (*aa == *bb) {
    return 0;
  } else {
    return 1;
  }
}

int laplace(st_spmv *spmv, int nx, int ny, int nz,
            int *p_n, int **p_rowptr, int **p_colind, double **p_nzvals,
            double **evals) {

    int proc_id   = -1;
    int num_procs =  0;
    if (spmv != NULL) {
        proc_id   = spmv->proc_id;
        num_procs = spmv->num_procs;
    }

    // global dimension
    int n   = (nz > 1 ? nx * ny * nz : nx * ny);
    int nnz = (nz > 1 ? 7*n : 5*n);
    if (proc_id == 0) {
        printf( " nx=%d, ny=%d, nz=%d, n=%d\n",nx,ny,nz,n );
        fflush(stdout);
    }
#if 0
    spmv->M = spmv->N = n;
    spmv->NNZ = nnz;

    // matrix distribution
    spmv->first_row = (int*)malloc((num_procs+1) * sizeof(int));
    spmv->count_row = (int*)malloc((num_procs+1) * sizeof(int));

    int p, i, k;
    int frow;
    int mloc = (spmv->M + num_procs-1)/ num_procs;
    spmv->first_row[0] = 0;
    for (p=1; p<=num_procs; p++) {
        spmv->first_row[p] = min(p*mloc, spmv->M);
        spmv->count_row[p-1] = spmv->first_row[p] - spmv->first_row[p-1];
        //if (proc_id == 0) printf( " first_row[%d] = %d, count_row[%d] = %d\n",p,  spmv->first_row[p],
        //                                                                      p-1,spmv->count_row[p-1] );
    }

    frow = spmv->first_row[proc_id];
    mloc = spmv->count_row[proc_id];
    nnz = nz > 1 ? 7*mloc : 5*mloc;
    int *rowptr = (int*)malloc((mloc+1) * sizeof(int));
    int *colind = (int*)malloc( nnz     * sizeof(int));
    double *nzvals = (double*)malloc( nnz  * sizeof(double));
#endif
    int *rowptr = (int*)malloc((n+1) * sizeof(int));
    int *colind = (int*)malloc( nnz  * sizeof(int));
    double *nzvals = (double*)malloc( nnz  * sizeof(double));
    if (rowptr == NULL || colind == NULL || nzvals == NULL) {
        printf( " failed to allocate rowptr, colind, or nzvals for laplace\n" );
        return -1;
    }

    double start_time = MPI_Wtime();
    nnz = 0;
    rowptr[0] = 0;
    for (int ii = 0; ii < n; ii++) {
        double v = -1.0;
        int i, j, k, jj;
        k = ii / (nx*ny);
        i = (ii - k*nx*ny) / nx;
        j = ii - k*nx*ny - i*nx;

        if (k > 0) {
            jj = ii - nx * ny;
            colind[nnz] = jj;
            nzvals[nnz] = v;
            nnz++;
        }
        if (k < nz-1) {
            jj = ii + nx * ny;
            colind[nnz] = jj;
            nzvals[nnz] = v;
            nnz++;
        }

        if (i > 0) {
            jj = ii - nx;
            colind[nnz] = jj;
            nzvals[nnz] = v;
            nnz++;
        }
        if (i < ny-1) {
            jj = ii + nx;
            colind[nnz] = jj;
            nzvals[nnz] = v;
            nnz++;
        }

        if (j > 0) {
            jj = ii - 1;
            colind[nnz] = jj;
            nzvals[nnz] = v;
            nnz++;
        }
        if (j < nx-1) {
            jj = ii + 1;
            colind[nnz] = jj;
            nzvals[nnz] = v;
            nnz++;
        }

        v = nz > 1 ? 6.0 : 4.0;
        colind[nnz] = ii;
        nzvals[nnz] = v;
        nnz++;

        rowptr[ii+1] = nnz;
    }
    double end_time = MPI_Wtime();
    if (proc_id == 0) printf( " > matrix generation: %.2e second\n",end_time-start_time );
    *p_rowptr = rowptr;
    *p_colind = colind;
    *p_nzvals = nzvals;
    *p_n = n;

    // compute exact eigenvalues
    start_time = MPI_Wtime();
    nnz = 0;
    double thetax = 0.5 * M_PI / (nx + 1.0);
    double thetay = 0.5 * M_PI / (ny + 1.0);
    double thetaz = 0.5 * M_PI / (nz + 1.0);
    (*evals) = (double*)malloc(n * sizeof(double));
    for (int i = 1; i <= nx; i++) {
        double tx = sin(i*thetax);
        for (int j = 1; j <= ny; j++) {
            double ty = sin(j*thetay);
            for (int k = 1; k <= nz; k++) {
                double tz = sin(k*thetaz);
                if (nz <= 1) {
                    tz = 0.0;
                }
                (*evals)[nnz++] = 4.0*(tx*tx+ty*ty+tz*tz);
                //printf( " eval[%d]=%.2e (tx=%.2e, ty=%.2e, tz=%.2e)\n",nnz-1,(*evals)[nnz-1],tx,ty,tz );
            }
        }
    }
    qsort(*evals, n, sizeof(double), compareDouble);
    end_time = MPI_Wtime();
    if (proc_id == 0) printf( " > eval generation  : %.2e second\n\n",end_time-start_time );
    fflush(stdout);

    return nnz;
}
