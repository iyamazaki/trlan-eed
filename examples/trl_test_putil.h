
#include "mpi.h"

#ifndef TRL_TEST_PUTIL
#define TRL_TEST_PUTIL

typedef struct {
    int proc_id;
    int num_procs;

    int M;
    int N;
    int NNZ;
    int *RowPtr;
    int *ColInd;
    double *NzVal;

    int m;
    int *first_row;
    int *count_row;

    int *rowptr;
    int *colind;
    double *nzval;

    MPI_Request *send_req;
    MPI_Request *recv_req;

    int *num_recvs;
    int **recv_vecs;
    double *recv_buffer;

    int *num_sends;
    int **send_vecs;
    double *send_buffer;

    double *x;
} st_spmv;

int  expand_ijv(FILE *fp, int *n, int *m, int *nnz, int **rowptr, int **colind, double **nzval );
int pexpand_ijv(FILE *fp, int *n, int *m, int *nnz, int **rowptr, int **colind, double **nzval );

int laplace(st_spmv *spmv, int nx, int ny, int nz,
            int *p_n, int **p_rowptr, int **p_colind, double **p_nzvals,
            double **evals);
#endif
