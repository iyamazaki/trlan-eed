#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "mpi.h"
#include "trlan.h"
#include "trl_map.h"

#include "trl_test_util.h"
#include "trl_test_putil.h"
#include "trl_test_eed.h"
#include "analyze_dist.h"

///
/// ---------------------------------------------------------------
/// a really simple example of how to use nuTRLan (parallel)
int main( int argc, char **argv ) {
    int lohi     = -1;     // compute smallest eigenvalues
    int scheme   = 1;      // restart scheme, keep 'fixed' number of Ritz pairs
    int nrow     = 30;     // matrix dimension
    int ned      = 10;     // number of eigenvalues desired
    int inc      = 5;      // number of eigenvalues computed at a time
    int maxlan   = 30;     // restart cycle
    int mev      = 30;     // size of evec, should be same as maxlan for using CA
    int s        = 0;      // step size
    bool rand_v0 = false;  // start with random vector or ones
    bool use_mpk = false;  // standard or s-step
    double tol   = 1.4901e-8;
    double gap   = 0.4; // min gap between kept Ritz values (may want smaller gap for computing a fewer eigenvalues at a time)
    FILE *fp = NULL;
    st_eed *eed = (st_eed*)malloc(sizeof(st_eed));
    eed->spmv = NULL;

    // initialize MPI
    if( MPI_Init(&argc, &argv) != MPI_SUCCESS ) {
        printf( "Failed to initialize MPI.\r\n" );
    }
    MPI_Comm_rank(MPI_COMM_WORLD, &(eed->mpi_rank));
    MPI_Comm_size(MPI_COMM_WORLD, &(eed->mpi_size));

    double *exact = NULL;
    int i, M;
    // default M
    MPI_Allreduce( &nrow, &M, 1,
                   MPI_INT, MPI_SUM, MPI_COMM_WORLD );
    for (int i=0; i<argc; i++) {
        if (strcmp(argv[i], "--nrow") == 0) {
            nrow = atoi( argv[++i] );
            MPI_Allreduce( &nrow, &M, 1,
                           MPI_INT, MPI_SUM, MPI_COMM_WORLD );
        }
        if (strcmp(argv[i], "--grow") == 0) {
            M = atoi( argv[++i] );
            int mloc = (M + eed->mpi_size-1)/ eed->mpi_size;
            nrow = min(M-(eed->mpi_rank*mloc), mloc);
        }
        if (strcmp(argv[i], "--ned") == 0) {
            ned = atoi( argv[++i] );
            mev = max(mev, ned);
        }
        if (strcmp(argv[i], "--maxlan") == 0) {
            maxlan = atoi( argv[++i] );
        }
        if (strcmp(argv[i], "--inc") == 0) {
            inc = atoi( argv[++i] );
        }
        if (strcmp(argv[i], "--s") == 0) {
            s = atoi( argv[++i] );
        }
        if (strcmp(argv[i], "--mpk") == 0) {
            use_mpk = true;
        }
        if (strcmp(argv[i], "--scheme") == 0) {
            scheme = atoi( argv[++i] );
        }
        if (strcmp(argv[i], "--random") == 0) {
            rand_v0 = true;
        }
        if (strcmp(argv[i], "--tol") == 0) {
            sscanf( argv[++i], "%lf", &tol );
        }
        if (strcmp(argv[i], "--gap") == 0) {
            sscanf( argv[++i], "%lf", &gap );
        }
        if (strcmp(argv[i], "--nx") == 0) {
            int nx = atoi( argv[++i] );
            st_spmv *spmv = (st_spmv*)malloc(sizeof(st_spmv));
            spmv->proc_id   = eed->mpi_rank;
            spmv->num_procs = eed->mpi_size;

            // TODO: distribute the matrix generation?
            int *rowptr;
            int *colind;
            double *nzvals;
            int nnz = laplace(spmv, nx, nx, 1, &nrow, &rowptr, &colind, &nzvals, &exact);
	    if (nnz < 0) return 0;
            M = nrow;

            if (eed->mpi_size > 1) {
                spmv->M   = nrow;
                spmv->N   = nrow;
                spmv->NNZ = nnz;
                spmv->RowPtr = rowptr;
                spmv->ColInd = colind;
                spmv->NzVal  = nzvals;
                // partition matrix
                partition_spmv(&nrow, &nnz, spmv);

                // analyze matrix
                analyze_spmv(spmv);
                spmv->x = (double*)malloc(spmv->M * sizeof(double));

                // free global
                free(rowptr); spmv->RowPtr = NULL;
                free(colind); spmv->ColInd = NULL;
                free(nzvals); spmv->NzVal = NULL;
            } else {
                spmv->first_row = (int*)malloc(2 * sizeof(int));
                spmv->count_row = (int*)malloc(1 * sizeof(int));
                spmv->first_row[0] = 0;
                spmv->first_row[1] = nrow;
                spmv->count_row[0] = nrow;

                spmv->rowptr = rowptr;
                spmv->colind = colind;
                spmv->nzval  = nzvals;
            }
            eed->spmv = spmv;
        }
        if (strcmp(argv[i], "--filename") == 0) {
            FILE *fp = fopen(argv[++i], "r");
            if (!fp) {
                printf( " failed to open %s\n",argv[i]);
            } else {
                st_spmv *spmv = (st_spmv*)malloc(sizeof(st_spmv));

                spmv->proc_id   = eed->mpi_rank;
                spmv->num_procs = eed->mpi_size;
                if (eed->mpi_size > 1) {
                    // read-in global matrix
                    expand_ijv(fp, &(spmv->M), &(spmv->N), &(spmv->NNZ),
                                   &(spmv->RowPtr), &(spmv->ColInd), &(spmv->NzVal) );
                    if (eed->mpi_rank== 0) {
                        printf( " > read %s <\n",argv[i] );
                        printf( " m=%d, n=%d, nnz=%d\n",spmv->M,spmv->N,spmv->NNZ );
                    }
                    M = spmv->M;

                    // partition matrix
                    int nnz;
                    partition_spmv(&nrow, &nnz, spmv);
                } else {
                    int nnz;
                    expand_ijv(fp, &nrow, &nrow, &nnz, &(spmv->rowptr), &(spmv->colind), &(spmv->nzval) );
                    spmv->first_row = (int*)malloc(2 * sizeof(int));
                    spmv->count_row = (int*)malloc(1 * sizeof(int));
                    spmv->first_row[0] = 0;
                    spmv->first_row[1] = nrow;
                    spmv->count_row[0] = nrow;
                    if (eed->mpi_rank== 0) {
                        printf( " > read %s <\n",argv[i] );
                        printf( " m=%d, n=%d, nnz=%d\n",nrow,nrow,nnz );
                    }
                    M = nrow;
                }
                eed->spmv = spmv;
            }
        }
        if (strcmp(argv[i], "--evalFilename") == 0) {
            FILE *fp = fopen(argv[++i], "r");
            if (!fp) {
                printf( " failed to open %s\n",argv[i]);
            } else {
                int me = read_dense( fp, &exact );
                printf( " > read %d exact evals from %s <\n",me,argv[i] );
            }
        }
    }
    if (eed->spmv != NULL && eed->mpi_size > 1 && eed->spmv->RowPtr != NULL) {
        // analyze matrix
        st_spmv *spmv = eed->spmv;
        analyze_spmv(spmv);
        spmv->x = (double*)malloc(spmv->M * sizeof(double));
    }
    maxlan = max(2*inc, maxlan);
    //mev = ned+maxlan-inc+1; // +1 for storing the last basis vector
    mev = ned+maxlan+1; // +1 for storing the last basis vector
    /*{
        int ii, jj;
        FILE *fp = fopen("A.dat","w");
        for( ii=0; ii<nrow; ii++ ) {
            for( jj=eed->spmv->rowptr[ii]; jj<eed->spmv->rowptr[ii+1]; jj++) {
                fprintf(fp,"%d %d %e\n",ii,eed->spmv->colind[jj],eed->spmv->nzval[jj]);
            }
        }
        fclose(fp);
    }*/

    // local variable declaration
    int lwrk=maxlan*(2*maxlan+10);
    double *eval  = (double*)malloc(sizeof(double) * mev);
    double *evec  = (double*)malloc(sizeof(double) * mev*nrow);

    double *res = (double*)malloc(sizeof(double) * lwrk);
    double *wrk = (double*)malloc(sizeof(double) * lwrk);
    if (eval == NULL || evec == NULL || res == NULL || wrk == NULL) {
        printf( " failed to allocate eval, evec, res, or wrk\n" );
        return 0;
    }
    if (exact == NULL) {
        exact = (double*)malloc(sizeof(double) * mev);
        for(int i=0; i<mev; i++) {
            exact[i] = getEval(i+1);
        }
    }
    // initialize info -- tell TRLAN to compute NEV smallest eigenvalues
    int nc, k;
    int check;
    trl_info info;
    eed->k     = 0;
    eed->s     = s;
    eed->nloc  = nrow;
    eed->U     = evec; 
    eed->eval  = eval; 
    eed->ldu   = nrow;
    eed->t     = NULL;
    eed->W     = NULL;
    for (nc=0, k=0; nc<ned; k++) {
        inc = min(inc, ned-nc);
        if (maxlan+1 > mev-nc) {
            if (eed->mpi_rank == 0) printf( " shrink maxlan=%d to be mev-nc=%d-%d\n",maxlan,mev,nc );
            maxlan = min(mev-nc, maxlan)-1;
        }
        trl_init_info( &info, nrow, maxlan, lohi, inc, tol, 7, 10*M, -1 );
        trl_set_iguess( &info, 0, 1, 0, NULL );
        // > Initialize EED parameters
        info.restart = scheme; // restart scheme
        info.gap     = gap; // restart scheme
        #ifdef NEWTON_BASIS
         #if ( MPK_VERSION == 1 )
         info.update_shifts = 1;
         if (eed->mpi_rank == 0) printf( " >> update shifts at each restart <<\n" );
         #else
         info.update_shifts = 1;
         if (eed->mpi_rank == 0) printf( " >> reuse shifts from first restart <<\n" );
         #endif
        #endif
        // > data structure for EED
        info.mvparam = eed;
        // the Lanczos recurrence is set to start with [1,1,...,1]^T
        if (rand_v0) {
            int ione = 1;
            int seed[] = {0, 0, 0, 1};
            double *global_evec = (double*)malloc(M * sizeof(double));
            dlarnv_(&ione, seed, &M, global_evec);

            int fst_row = (eed->mpi_size == 1 ? 0 : eed->spmv->first_row[eed->mpi_rank]);
            for (int i = 0; i < nrow; i++) {
                evec[nrow*nc + i] = global_evec[fst_row + i];
            }
            free(global_evec);
        } else {
            for(int i=0; i<nrow; i++) {
                evec[nrow*nc+i] = 1.0;
            }
        }
        memset( &eval[nc], 0, (mev-nc)*sizeof(double) );
        // > Initialize EED parameters
        eed->k  = nc;
        eed->ld = nc;
        eed->time_spmv = 0.0;
        if (nc > 0) {
            //eed->alpha = getEval(nrow/2);
            eed->alpha = eval[nc-1] + (info.max_eval - eval[nc-1])/2.0;
            if (eed->mpi_rank== 0) {
                printf( " max(Eval)=%.2e Eval[%d]=%.2e -> alpha=%.2e\n",
                        info.max_eval,nc-1,eval[nc-1],eed->alpha );
                fflush(stdout);
            }
            if (eed->t != NULL) {
                free(eed->y);
                free(eed->t);
                free(eed->b);
                if (eed->W != NULL) {
                    free(eed->W);
                }
            }
            eed->y = (double*)malloc(nc * sizeof(double));
            if (s == 0) {
                eed->t = (double*)malloc(2*max(nrow, nc) * sizeof(double));
                eed->b = (double*)malloc(2*nc * sizeof(double));
            } else {
                eed->t = (double*)malloc(max(2*nc,nrow*s) * sizeof(double));
                eed->b = (double*)malloc(s*nc * sizeof(double));
            }
            // setup auxiriary matrices for CA low-rank
            setup_mpk(eed, info.mvparam);
        } else {
            // used to store the last (residual) vector
            info.rr = (double*)malloc(nrow * sizeof(double));
        }
        // > Initialize s-step parameters
        info.use_mpk = use_mpk;
        info.mks = s;
        if (eed->mpi_rank == 0) {
            char logfile[100];
            sprintf(logfile,"LOG%d_",k);
            //sprintf(logfile,"OUT%d_",k);
            trl_set_debug( &info, 0, logfile );
            //info.verbose =  1;
            //info.verbose =  8;
        }
        // > Call TRLAN to compute the eigenvalues
#if 0
        info.restart    = 9;
        info.inc_nec    = 100;
        info.maxlan_inc = min(200, maxlan);
        //info.inc_nec    = 200;
        //info.maxlan_inc = min(400, maxlan);
        //info.inc_nec    = 400;
        //info.maxlan_inc = min(800, maxlan);
#endif
        MPI_Barrier(MPI_COMM_WORLD);
        if (s > 0) {
            if (eed->mpi_rank == 0) printf( " CA-TRLan(s=%d, inc=%d, mev=%d, maxlan=%d)\n",s,inc,mev-nc,info.maxlan );
            fflush(stdout);
            if (use_mpk == true) {
                //trlan(mpk_standard, &info, nrow, mev-nc, &eval[nc], &evec[nrow*nc], nrow, lwrk, res );
                //trlan(mpk_check, &info, nrow, mev-nc, &eval[nc], &evec[nrow*nc], nrow, lwrk, res );
                trlan(mpk, &info, nrow, mev-nc, &eval[nc], &evec[nrow*nc], nrow, lwrk, res );
            } else {
                trlan(spmv, &info, nrow, mev-nc, &eval[nc], &evec[nrow*nc], nrow, lwrk, res );
            }
        } else {
            if (eed->mpi_rank == 0) printf( " TRLan(inc=%d, mev=%d, maxlan=%d)\n",inc,mev-nc,info.maxlan );
            fflush(stdout);
            trlan(spmv, &info, nrow, mev-nc, &eval[nc], &evec[nrow*nc], nrow, lwrk, res );
        }
        #if 0
        {
            FILE *fp;
            char outfilename[100];
            sprintf(outfilename,"out%d_%d.m",eed->mpi_rank,k);
            fp = fopen(outfilename, "w");

            int ii, jj;
            fprintf(fp, " eval%d=[",eed->mpi_rank );
            for (jj=0; jj<info.nec; jj++) {
                fprintf(fp, "%.16e ",eval[nc+jj]);
            }
            fprintf(fp, "];\n" );
            fprintf(fp, " evec%d=[",eed->mpi_rank );
            for (ii=0; ii<nrow; ii++) {
                for (jj=0; jj<info.nec; jj++) {
                    fprintf(fp, "%.16e ",evec[ii + (nc+jj)*nrow]);
                }
                fprintf(fp, "\n" );
            }
            fprintf(fp, "];\n" );
            /*fprintf(fp, " rr=[" );
            for (ii=0; ii<nrow; ii++) {
                fprintf(fp, "%.16e\n",info.rr[ii]);
            }
            fprintf(fp, "];\n" );*/

            fclose(fp);
        }
        #endif
        if (info.stat != 0) {
            printf( "\n ** failed with stat=%d **\n\n",info.stat );
            break;
        } else if (info.nec < inc) {
            printf( "\n ** failed to compute %d eigenvalues (nec=%d) **\n\n",inc,info.nec );
            break;
        }
        // flop count for each SpMV+EED
        int mvflop;
        if (eed->spmv != NULL) {
            mvflop = 2*eed->spmv->rowptr[nrow];
        } else {
            mvflop = 2*nrow; // diagonal
        }
        mvflop += 4*nrow*nc;
        if (eed->mpi_rank == 0) printf( " mvflop=%d (nrow=%d, nc=%d)\n\n",mvflop,nrow,nc );

        // update number of convergence
        nc += info.nec;
        //nc += inc;
        MPI_Barrier(MPI_COMM_WORLD);
        if (eed->mpi_rank == 0) printf( " %d converged (total=%d)\n\n",info.nec,nc );
        fflush(stdout);

        info.tick_e = eed->time_spmv;
        trl_print_info( &info, mvflop );
        //trl_terse_info( &info, 0 );
        eed->k = 0;
        trl_check_ritz(spmv, &info, nrow, nc, evec, nrow, eval, &check, res,
                       exact, wrk, lwrk);
        if( info.verbose > 1 ) {
            trl_rayleigh_quotients( spmv, &info, nc, evec, nrow, res, NULL );
            trl_check_ritz(spmv, &info, nrow, nc, evec, nrow, res, &check,
                           &res[i], exact, wrk, lwrk);
        }
        if (info.stat != 0) {
            printf( " failed with stat=%d\n",info.stat );
            break;
        } else if (info.nec < inc) {
            printf( " failed to compute %d eigenvalues (nec=%d)\n",inc,info.nec );
            break;
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);
    free(res); free(wrk);
    free(eval); free(evec); free(exact);
    MPI_Finalize();
}
