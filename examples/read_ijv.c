#include <stdlib.h>
#include <stdio.h>

#include "trlan.h"
#include "trl_test_util.h"

int read_dense( FILE *fp, double **exact ) {

    int m;
    fscanf( fp, "%d",&m );
    (*exact) = (double*)malloc( m * sizeof(double) );

    int i;
    for( i=0; i<m; i++ ) {
        double val;
        fscanf( fp, "%lf",&val );
        (*exact) [i] = val;
    }
    fclose(fp);
    return m;
}


int read_ijv(FILE *fp, int *n, int *m, int *nnz, int **rowptr, int **colind, double **nzval ) {
    int i, count, row, col, prev_row, c_format, nnz0;
    //float val;
    double val;

    fscanf( fp, "%d %d %d",n,m,nnz );
    //printf( " m=%d n=%d nnz=%d.\n",*m,*n,*nnz );

    (*rowptr) = (int*)malloc( (*n+1) * sizeof(int) );
    (*colind) = (int*)malloc(  *nnz  * sizeof(int) );
    (*nzval)  = (double*)malloc( *nnz * sizeof(double) );
    fscanf( fp, "%d %d %lf",&row,&col,&val );
    if( row == 0 ) {
        c_format = 0;
    } else {
        c_format = -1;
    }

    (*rowptr)[0] = 0;
    (*colind)[0] = col + c_format;
    (*nzval) [0] = val;
    count = 1;
    prev_row = row;
    for( i=1; i<*nnz; i++ ) {
       fscanf( fp, "%d %d %lf",&row,&col,&val );

       // check again!!
       if( row < prev_row ) {
           printf( " stored in column-major (%d < %d)??\n",row,prev_row );
           fclose(fp);
           return -(row+1);
       }

       (*colind)[count] = col + c_format;
       (*nzval) [count] = val;
       if( prev_row != row ) {
           (*rowptr)[prev_row+c_format+1] = count;
           prev_row                       = row;
       }
       count ++;
    }
    (*rowptr)[*n] = count;
    fclose(fp);

    return 0;
}



int expand_ijv(FILE *fp, int *n, int *m, int *nnz, int **rowptr, int **colind, double **nzval ) {
    int i, count, row, col, prev_row, c_format, nnz0;
    //float val;
    double val;

    fscanf( fp, "%d %d %d",n,m,&nnz0 );
    //printf( " m=%d n=%d nnz=%d.\n",*m,*n,nnz0 );
    *nnz = 2 * nnz0 - *n;

    (*rowptr) = (int*)   calloc((*n+1),   sizeof(int) );
    (*colind) = (int*)   malloc( *nnz   * sizeof(int) );
    (*nzval)  = (double*)malloc( *nnz   * sizeof(double) );

    c_format = -1;
    //c_format = 0;
    for( i=0; i<nnz0; i++ ) {
        fscanf( fp, "%d %d %lf",&row,&col,&val );
        (*rowptr)[col + c_format] ++;
        if( col != row )
            (*rowptr)[row + c_format] ++;
    }

    for( i=0; i<*n; i++ ) (*rowptr)[i+1] += (*rowptr)[i];
    for( i=*n; i>0; i-- ) (*rowptr)[i]    = (*rowptr)[i-1];
    (*rowptr)[0] = 0;

    rewind(fp);
    fscanf( fp, "%d %d %d",n,m,&nnz0 );
    for( i=0; i<nnz0; i++ ) {
        fscanf( fp, "%d %d %lf",&row,&col,&val );

        (*nzval) [(*rowptr)[col + c_format]] = val;
        (*colind)[(*rowptr)[col + c_format]] = row + c_format;
        (*rowptr)[col + c_format] ++;

        if( col != row ) {
            (*nzval) [(*rowptr)[row + c_format]] = val;
            (*colind)[(*rowptr)[row + c_format]] = col + c_format;
            (*rowptr)[row + c_format] ++;
        }
    }
    for( i=*n; i>0; i-- ) (*rowptr)[i]    = (*rowptr)[i-1];
    (*rowptr)[0] = 0;

    fclose(fp);

    return 0;
}

