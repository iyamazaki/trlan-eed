#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "mpi.h"
#include "trlan.h"
#include "trl_map.h"

#include "analyze_dist.h"
#include "trl_test_util.h"

#if !defined(TEST_EED_H)
#define TEST_EED_H
//
// This file contains an example to compute parallel nuTRLan to compute
// eigenpairs of a symmetric matrix.
typedef struct {
  // process info
  int mpi_rank;
  int mpi_size;

  // csr
  int n, frow, nloc, nnz;
  st_spmv *spmv;

  // eed
  int    k;
  double alpha;
  double *U;
  double *eval;
  int    ldu;
  double *t;
  double *b;
  double *y;

  // ca
  int s;
  double *W;
  double *Z;
  double *T;
  int ld;

  // Ritz shift for Newton bases (computed in TRLan)
  double *shifts;

  // time
  double time_spmv;
} st_eed;


///
/// ---------------------------------------------------------------
// a simple matrix-vector multiplications routine with
// a diagonal matrix with values (1, 4, 9, 16, 25, ....)
double getEval(int i) {
    //return (double)(i*i);
    return (double)(i);
}

///
/// ---------------------------------------------------------------
// Matrix operation
void op(const int nrow,
        const double *xin,
              double *yout, 
        void *mvparam) {


    int i, j;
    st_eed *eed = (st_eed*)mvparam;

    double time_start = MPI_Wtime();
    if (eed->spmv == NULL) {
        for( i=0; i<nrow; i++ ) {
            yout[i] = getEval(i+1.0)*xin[i];
        }
    } else {
        st_spmv *spmv = eed->spmv;
        for( i=0; i<nrow; i++ ) {
            yout[i] = 0.0;
            for( j=spmv->rowptr[i]; j<spmv->rowptr[i+1]; j++) {
                yout[i] += spmv->nzval[j]*xin[spmv->colind[j]];
            }
        }
    }
    eed->time_spmv += (MPI_Wtime() - time_start);
}

// Parallel matrix operation
void pop(const int nrow, 
         const double *xin, 
               double *yout,
         void *mvparam) {
    //
    // ..
    // .. local variables ..
    st_eed *eed = (st_eed*)mvparam;
    //
    int i;
    //
    // ..
    // .. executable statements ..
    int iproc = eed->mpi_rank;
    int nproc = eed->mpi_size;

    double time_start = MPI_Wtime();
    if (eed->spmv == NULL) {
        int doff = iproc*nrow;
        for( i=0; i<nrow; i++ ) {
            yout[i] = getEval(doff+i+1.0)*xin[i];
        }
    } else {
        int *disp, *count;
        double *lxin, *gxin;

        st_spmv *spmv = eed->spmv;
#define P2P_SPMV
#if defined(P2P_SPMV)
        gxin = spmv->x;
        trl_dcopy(spmv->count_row[iproc], xin, 1, &(gxin[spmv->first_row[iproc]]), 1);
        startP2P(gxin, spmv);
        waitP2P(gxin, spmv);
#else
  #if 0
        count  = malloc(    nproc  * sizeof(int) );
        disp   = malloc( (1+nproc) * sizeof(int) );
        MPI_Allgather( &nrow, 1, MPI_INT, count, 1, MPI_INT, MPI_COMM_WORLD );
        disp[0] = 0;
        for( i=0; i<nproc; i++ ) disp[i+1] = disp[i] + count[i];
  #else
        disp  = spmv->first_row;
        count = spmv->count_row;
  #endif
        lxin = (double*)xin;
        gxin = malloc( (spmv->M) * sizeof(double) );
        MPI_Allgatherv( lxin, nrow, MPI_DOUBLE, gxin, count, disp, MPI_DOUBLE, MPI_COMM_WORLD );
#endif

        for( i=0; i<spmv->m; i++ ) {
            int k;
            yout[i] = 0.0;
            for( k=spmv->rowptr[i]; k<spmv->rowptr[i+1]; k++ )
                yout[i] += spmv->nzval[k] * gxin[spmv->colind[k]];
        }
#if !defined(P2P_SPMV)
        free(gxin);
  #if 0
        free(count);
        free(disp);
  #endif
#endif
    }
    eed->time_spmv += (MPI_Wtime() - time_start);
}

///
/// ---------------------------------------------------------------
// SpMV
void spmv(const int nrow, const int ncol, 
          const double *xin,  const int ldx,
                double *yout, const int ldy,
          void *mvparam) {
    // local variables
    int i, j;
    int ioff, joff;
    st_eed *eed = (st_eed*)mvparam;

    int doff;
    if (eed->spmv != NULL) {
        doff = eed->spmv->first_row[eed->mpi_rank];
    } else {
        MPI_Comm_rank(MPI_COMM_WORLD, &doff);
        doff *= nrow;
    }
    for( j=0; j<ncol; j++ ) {
        // yout = A*xin
        ioff = j*ldx;
        joff = j*ldy;
        #define USE_OP
        #if defined(USE_OP)
        if (eed->mpi_size > 1) {
            pop(nrow, &xin[ioff], &yout[joff], mvparam);
        } else {
            op(nrow, &xin[ioff], &yout[joff], mvparam);
        }
        #else
        for( i=0; i<nrow; i++ ) {
            yout[joff+i] = getEval(doff+i+1.0)*xin[ioff+i];
        }
        #endif

        // yout = yout + alpha * U * (U'*xin)
        if (eed->k > 0) {
            // > compute t = U'*xin
            const double one  = 1.0;
            const double zero = 0.0;
            if (eed->mpi_size > 1) {
                trl_dgemv("T", nrow, eed->k,
                          one,  eed->U, eed->ldu,
                                &xin[ioff], 1,
                          zero, &(eed->t[eed->k]), 1);
                MPI_Allreduce( &(eed->t[eed->k]), eed->t, eed->k, 
                               MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );
            } else {
                trl_dgemv("T", nrow, eed->k,
                          one,  eed->U,    eed->ldu,
                               &xin[ioff], 1,
                          zero, eed->t,    1);
            }

            // > compute yout = yout + alpha * U*t 
            trl_dgemv("N", nrow, eed->k,
                      eed->alpha, eed->U,     eed->ldu,
                                  eed->t,     1,
                      one,       &yout[joff], 1);
        }
    }
}

///
/// ---------------------------------------------------------------
// standard MPK (no CA)
void mpk_standard(const int nrow, const int ncol, 
                  const double *shift, const int mks,
                        double *q,     const int ldq,
                  void *mvparam) {

    int i, k, i__1 = 1;
//printf( " MPK-standard(k=%d, mks=%d)\n",k,mks );
    for( k=0; k<mks; k++ ) {
        spmv(nrow, i__1, &q[k*ldq], ldq, &q[(k+1)*ldq], ldq, mvparam);

        #ifdef NEWTON_BASIS
        if (shift != NULL) {
//printf( " > shift[%d]=%e\n",k,shift[k] );
            for( i=0; i<nrow; i++ ) {
                q[(k+1)*ldq+i] -= shift[k]*q[k*ldq+i];
            }
        }
        #endif
    }
}

#define MPK_VERSION 1 // Bai's version
//#define MPK_VERSION 2 // Nick-Erin's version

#if ( MPK_VERSION == 1 )
///
/// ---------------------------------------------------------------
// CA MPK, Bai's version
    // check for errors
    void mpk_check(const int nrow, const int ncol, 
                   const double *shift, const int mks,
                         double *q,     const int ldq,
                   void *mvparam) {

        st_eed *eed = (st_eed*)mvparam;
        if (eed->k == 0) {
            mpk_standard(nrow, ncol, shift, mks, q, ldq, mvparam);
        } else {
            // run CA MKL
            int i, j;
            int nc  = eed->k;
            int ld  = eed->ld;
            int ldu = eed->ldu;
            double *U = eed->U;
            double *y = eed->y;
            double *b = eed->b;
            double alpha = eed->alpha;
            // b0 = U^T * q0
            trl_dgemv("T", nrow, nc,
                      1.0, U, ldu,
                           q, 1,
                      0.0, y, 1);
            for (i=0; i<nc; i++) {
                b[i] = alpha*y[i];
            }

            // b(j) = W(j)*b(0)
            for (j=0; j<mks-1; j++) {
                trl_dgemv("T", nc, nc,
                          1.0, &eed->W[j*(ld*ld)], ld,
                               b, 1,
                          0.0, &b[(j+1)* nc], 1);
            }

            // t(j) = alpha * U * b(j)
            double *t = eed->t;
            trl_dgemm("N", "N",
                      nrow, mks, nc,
                      1.0, U, ldu,
                           b, nc,
                      0.0, t, nrow);

            // q(j) = A*q(j-1) + t(j-1)
            int doff;
            if (eed->spmv != NULL) {
                doff = eed->spmv->first_row[eed->mpi_rank];
            } else {
                MPI_Comm_rank(MPI_COMM_WORLD, &doff);
                doff *= nrow;
            }
            for( j=0; j<mks; j++ ) {
                #if defined(USE_OP)
                if (eed->mpi_size > 1) {
                    pop(nrow, &q[j*ldq], &q[(j+1)*ldq], mvparam);
                } else {
                    op(nrow, &q[j*ldq], &q[(j+1)*ldq], mvparam);
                }
                #else
                for( i=0; i<nrow; i++ ) {
                    q[i+(j+1)*ldq] = getEval(doff+i+1.0)*q[i+j*ldq];
                }
                #endif

                for( i=0; i<nrow; i++ ) {
                    q[i+(j+1)*ldq] += t[i+j*nrow];
                }
            }
            #if 1
            // run standard MKL to check against
            double *q_std = (double*)malloc(ldq*(1+mks) * sizeof(double));
            trl_dcopy(nrow, q, 1, q_std, 1);
            mpk_standard(nrow, ncol, shift, mks, q_std, ldq, mvparam);
            for( j=0; j<mks; j++ ) {
                double qnorm = 0.0;
                double enorm = 0.0;
                for( i=0; i<nrow; i++ ) {
                    double error = q[i+(j+1)*ldq] - q_std[i+(j+1)*ldq];
                    qnorm += q_std[i+(j+1)*ldq] * q_std[i+(j+1)*ldq];
                    enorm += error*error;
                }
                printf("%d %.2e %.2e\n",j,sqrt(enorm),sqrt(qnorm));
            }
            printf( "\n" );
            free(q_std);
            #endif
        }
    }

    // >> MPK <<
    void mpk(const int nrow, const int ncol, 
             const double *shift, const int mks,
                   double *q,     const int ldq,
             void *mvparam) {

        st_eed *eed = (st_eed*)mvparam;
        //mpk_standard(nrow, ncol, shift, mks, q, ldq, mvparam);
        //return;

        if (eed->k == 0) {
            mpk_standard(nrow, ncol, shift, mks, q, ldq, mvparam);
        #ifdef NEWTON_BASIS
        } else if( shift == NULL ) {
	    //printf( " shift == NULL\n" );
            mpk_standard(nrow, ncol, shift, mks, q, ldq, mvparam);
        #endif
        } else {
            int i, j;
            int nc  = eed->k;
            int ld  = eed->ld;
            int ldu = eed->ldu;
            double *U = eed->U;
            double *y = eed->y;
            double *b = eed->b;
            double alpha = eed->alpha;

            // b0 = U^T * q0
            trl_dgemv("T", nrow, nc,
                      1.0, U, ldu,
                           q, 1,
                      0.0, y, 1);
            if (eed->mpi_size > 1) {
                MPI_Allreduce( y, b, nc, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );
                for (i=0; i<nc; i++) {
                    b[i] *= alpha;
                }
            } else {
                for (i=0; i<nc; i++) {
                    b[i] = alpha*y[i];
                }
            }

            // b(j) = W(j)*b(0)
            for (j=0; j<mks-1; j++) {
		#if 0
                trl_dgemv("T", nc, nc,
                          1.0, &eed->W[j*(ld*ld)], ld,
                               b, 1,
                          0.0, &b[(j+1)* nc], 1);
                #else
		for (int k = 0; k < nc; k++) {
		    b[k + (j+1)* nc] = eed->W[(k+k*ld) + j*(ld*ld)] * b[k];
		}
                #endif
            }

            // t(j) = alpha * U * b(j)
            double *t = eed->t;
            trl_dgemm("N", "N",
                      nrow, mks, nc,
                      1.0, U, ldu,
                           b, nc,
                      0.0, t, nrow);

            // q(j) = A*q(j-1) + t(j-1)
            int doff;
            if (eed->spmv != NULL) {
                doff = eed->spmv->first_row[eed->mpi_rank];
            } else {
                MPI_Comm_rank(MPI_COMM_WORLD, &doff);
                doff *= nrow;
            }
            for( j=0; j<mks; j++ ) {
                #if defined(USE_OP)
                if (eed->mpi_size > 1) {
                    pop(nrow, &q[j*ldq], &q[(j+1)*ldq], mvparam);
                } else {
                    op(nrow, &q[j*ldq], &q[(j+1)*ldq], mvparam);
                }
                #else
                for( i=0; i<nrow; i++ ) {
                    q[i+(j+1)*ldq] = getEval(doff+i+1.0)*q[i+j*ldq];
                }
                #endif
                #ifdef NEWTON_BASIS
                if (shift != NULL) {
                    for( i=0; i<nrow; i++ ) {
                        q[i+(j+1)*ldq] -= shift[j]*q[i+j*ldq];
                    }
                }
                #endif

                for( i=0; i<nrow; i++ ) {
                    q[i+(j+1)*ldq] += t[i+j*nrow];
                }
            }
        }
    }

    void setup_mpk(st_eed *eed, void *mvparam) {
        int i, j, k;
        int s  = eed->s;
        int nc = eed->k;
        int ld = eed->ld;
        double alpha = eed->alpha;
        double *eval = eed->eval;
        eed->W = (double*)calloc((s-1) * ld*ld, sizeof(double));

        if (eed->mpi_rank == 0) printf( " >> setting up specialized MPK <<\n" );
        #if !defined(NEWTON_BASIS)
        for (j = 0; j < s-1; j++) {
            // compute W(j)
            double *Wj = &(eed->W[j*ld*ld]);
            for(k = 0; k < nc; k++) {
                // compute k-th diagonal of W(i)
                Wj[k+k*ld] = pow(eval[k]+alpha, (double)(1+j));
            }
        }
        #endif
    }

    void init_mpk(void *mvparam, double *shift) {
        #ifdef NEWTON_BASIS
        int i, j, k, l;
        st_eed *eed = (st_eed*)mvparam;

        int s  = eed->s;
        int nc = eed->k;
        int ld = eed->ld;
        if (nc == 0) return;

        double alpha = eed->alpha;
        double *eval = eed->eval;
//printf( " init-MPK\n" ); fflush(stdout);
//for (j=0; j<s; j++) printf( " > shift[%d]=%e\n",j,shift[j] );
        for (j = 0; j < s-1; j++) {
            // compute W(j)
            double *Wj = &(eed->W[j*ld*ld]);
            for (k = 0; k < nc; k++) {
                // compute k-th diagonal of W(i)
                if (shift == NULL) {
                    Wj[k+k*ld] = pow(eval[k]+alpha, (double)(1+j));
                } else {
                    Wj[k+k*ld] = 1.0;
                    for (i = 0; i <= j; i++) {
                        Wj[k+k*ld] *= (eval[k]+alpha-shift[i]);
                    }
                }
            }
        }
//printf( " done init-MPK\n" ); fflush(stdout);
        #endif
    }
#else
///
/// ---------------------------------------------------------------
// CA MPK, Nick-Erin's version
    void mpk(const int nrow, const int ncol, 
             const double *shift, const int mks,
                   double *q,     const int ldq,
             void *mvparam) {

        st_eed *eed = (st_eed*)mvparam;
        if (eed->k == 0) {
            mpk_standard(nrow, ncol, shift, mks, q, ldq, mvparam);
        #ifdef NEWTON_BASIS
        } else if( shift == NULL ) {
	    // if Newton basis is requested but if no shifts are available (e.g., first restart loop)
            mpk_standard(nrow, ncol, shift, mks, q, ldq, mvparam);
        #endif
        } else {
            int i, j, k;
            int nc  = eed->k;
            int ld  = eed->ld;
            int ldu = eed->ldu;
            double *U = eed->U;
            double alpha = eed->alpha;

            // q(j) = A*q(j-1)
            int doff;
            if (eed->spmv != NULL) {
                doff = eed->spmv->first_row[eed->mpi_rank];
            } else {
                MPI_Comm_rank(MPI_COMM_WORLD, &doff);
                doff *= nrow;
            }
            for( j = 0; j < mks-1; j++ ) {
                #if defined(USE_OP)
                if (eed->mpi_size > 1) {
                    pop(nrow, &q[j*ldq], &q[(j+1)*ldq], mvparam);
                } else {
                    op(nrow, &q[j*ldq], &q[(j+1)*ldq], mvparam);
                }
                #else
                for( i=0; i<nrow; i++ ) {
                    q[i+(j+1)*ldq] = getEval(doff+i+1.0)*q[i+j*ldq];
                }
                #endif
                #ifdef NEWTON_BASIS
                if (shift != NULL) {
                    for( i=0; i<nrow; i++ ) {
                        q[i+(j+1)*ldq] -= shift[j]*q[i+j*ldq];
                    }
                }
                #endif
            }

            // b(j) = U' * q(0:mks)
            double *b = eed->b;
            trl_dgemm("T", "N",
                      nc, mks, nrow,
                      1.0, U, ldu,
                           q, ldq,
                      0.0, b, nc);

            // compute b
            double *W = eed->W;
            for( j = 0; j < mks; j++ ) {
                for( i=1; i<=j; i++ ) {
                    #ifdef NEWTON_BASIS
                    double *Zij = &(eed->Z[(j-i)*ld*ld + (i-1)*ld]);
                    for( k=1; k<=i; k++) {
                        double beta = Zij[k-1];
                        double *Wi = (double*)&(W[(k-1)*(ld*ld)]);
                        trl_dgemv("N", nc, nc,
                                  beta, Wi, ld,
                                        &b[(j-i)*nc], 1,
                                  1.0,  &b[ j   *nc], 1);
                    }
                    #else
                    double beta = 1.0;
                    double *Wi = (double*)&(W[(i-1)*(ld*ld)]);
                    trl_dgemv("N", nc, nc,
                              beta, Wi, ld,
                                    &b[(j-i)*nc], 1,
                              1.0,  &b[ j   *nc], 1);
                    #endif
                }
                trl_dscal(nc, alpha, &b[j*nc], 1);
            }

            // t=U*b
            double *t = eed->t;
            trl_dgemm("N", "N",
                      nrow, mks, nc,
                      1.0, U, ldu,
                           b, nc,
                      0.0, t, nrow);

            // q(j) = A*q(j-1)
            for( j = 0; j < mks; j++ ) {
                #if defined(USE_OP)
                if (eed->mpi_size > 1) {
                    pop(nrow, &q[j*ldq], &q[(j+1)*ldq], mvparam);
                } else {
                    op(nrow, &q[j*ldq], &q[(j+1)*ldq], mvparam);
                } 
                #else
                for( i=0; i<nrow; i++ ) {
                    q[i+(j+1)*ldq] = getEval(doff+i+1.0)*q[i+j*ldq];
                }
                #endif
                #ifdef NEWTON_BASIS
                if (shift != NULL) {
                    for( i=0; i<nrow; i++ ) {
                        q[i+(j+1)*ldq] -= shift[j]*q[i+j*ldq];
                    }
                }
                #endif

                for( i=0; i<nrow; i++ ) {
                    q[i+(j+1)*ldq] += t[i+j*nrow];
                }
            }
        }
    }

    void init_mpk(void *mvparam, double *shift) {
        st_eed *eed = (st_eed*)mvparam;
        int s  = eed->s;
        int nc = eed->k;
        int ld = eed->ld;
        int nrow = eed->nloc;
        if (nc == 0) return;

        double alpha = eed->alpha;
        double *eval = eed->eval;
        int i, j, k, l;
        double *W = eed->W;
        for (j = 0; j < s; j++) {
            // compute W(j)
            double *Wj = &W[j*ld*ld];
            for (k = 0; k < nc; k++) {
                // compute k-th diagonal of W(i)
                #ifdef NEWTON_BASIS
                if (shift == NULL) {
                    Wj[k+k*ld] = pow(eval[k], (double)(j));
                } else {
                    Wj[k+k*ld] = 1.0;
                    for (i=0; i<j; i++) {
                        Wj[k+k*ld] *= (eval[k]-shift[i]);
                    }
                }
                #else
                Wj[k + k*ld] = pow(eval[k], (double)(j));
                #endif
            }
        }
        #ifdef NEWTON_BASIS
        double *Z = eed->Z;
        for (j = 0; j < s; j++) {
            // compute W(j)
            double *Zj = &Z[j*ld*ld];
            Zj[0] = 1.0;
            //printf( " >> j=%d <<\n",j );
            for (i = 1; i < s-j-1; i++) {
                //printf( " -- i=%d --\n",i );
                Zj[i*ld] = 0.0;
                for (k = 0; k < i; k++) {
                    double beta = Zj[k + (i-1)*ld];
                    Zj[k+1 + i*ld]  = beta;
                    //printf( " z[%d,%d] = %.2e (%d,%d)\n",k+1,i,beta,k,i-1 );
                    //printf( " z[%d,%d] = %.2e + %.2e*(%.2e+%.2e)",k,i,Zj[k + i*ld],beta,shift[k],shift[j+i] );
                    Zj[k   + i*ld] += beta * shift[k];
                    Zj[k   + i*ld] -= beta * shift[j+i];
                    //printf( " = %.2e\n",Zj[k + i*ld] );
                }
            }
        }
        #endif
    }

    void setup_mpk(st_eed *eed, void *mvparam) {
        int s  = eed->s;
        int nc = eed->k;
        int ld = eed->ld;
        eed->W = (double*)calloc(s * ld*ld, sizeof(double));
        eed->Z = (double*)calloc(s * ld*ld, sizeof(double));
        if (eed->mpi_rank == 0) printf( " >> setting up general MPK <<\n" );
        #ifndef NEWTON_BASIS
        init_mpk(mvparam, NULL);
        #endif
    }
#endif
#endif
