#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "mpi.h"
#include "trlan.h"
#include "trl_map.h"

int pexpand_ijv(FILE *fp, int *n, int *m, int *nnz, int **rowptr, int **colind, double **nzval ) {
   int i, iproc, nproc, dn, n1, n2, nloc, count, row, col, prev_row, c_format, nnz0;
   double val;

   fscanf( fp, "%d %d %d",n,m,&nnz0 );
   printf( " m=%d n=%d nnz=%d.\n",*m,*n,nnz0 );
   MPI_Comm_rank(MPI_COMM_WORLD, &iproc);
   MPI_Comm_size(MPI_COMM_WORLD, &nproc);

   dn   = ceil( ((double)*n) / nproc );
   n1   = min( *n, iproc * dn );
   n2   = min( *n, n1 + dn );
   nloc = n2 - n1;
   printf( " # %d: dn=%d n1=%d n2=%d\n",iproc,dn,n1,n2 );

   assert( (*rowptr) = (int*)   calloc((*n+1),   sizeof(int) ) );

   fscanf( fp, "%d %d %lf",&row,&col,&val );
   if( row == 0 ) {
       c_format = 0;
   } else {
       c_format = -1;
   }
   *nnz = 0;
   if( ( col+c_format >= n1 ) && ( col+c_format < n2 ) ) {
    (*rowptr)[col + c_format] ++;
    (*nnz) ++;
   }
   if( col != row && (row+c_format >= n1 ) && (row+c_format < n2 ) ) {
     (*rowptr)[row + c_format] ++;
     (*nnz) ++;
   }

   for( i=1; i<nnz0; i++ ) {
      fscanf( fp, "%d %d %lf",&row,&col,&val );

      if( ( col+c_format >= n1 ) && ( col+c_format < n2 ) ) {
        (*rowptr)[col + c_format] ++;
        (*nnz) ++;
      }
      if( col != row && (row+c_format >= n1 ) && (row+c_format < n2 ) ) {
        (*rowptr)[row + c_format] ++;
        (*nnz) ++;
      }
   }
   printf( " # %d: nnz0=%d nnz=%d\n",iproc,nnz0,*nnz );

   for( i=n1; i<n2; i++ ) (*rowptr)[i+1] += (*rowptr)[i];
   for( i=n2; i<*n; i++ ) (*rowptr)[i]    = (*rowptr)[i-1];
   for( i=*n; i>0; i-- ) (*rowptr)[i]    = (*rowptr)[i-1];
   (*rowptr)[0] = 0;

   assert( ((*colind) = (int*)   malloc( *nnz   * sizeof(int) )) );
   assert( ((*nzval)  = (double*)malloc( *nnz   * sizeof(double) )) );

   rewind(fp);
   fscanf( fp, "%d %d %d",n,m,&nnz0 );
   for( i=0; i<nnz0; i++ ) {
      fscanf( fp, "%d %d %lf",&row,&col,&val );

      if( ( col+c_format >= n1 ) && ( col+c_format < n2 ) ) {
        (*nzval) [(*rowptr)[col + c_format]] = val;
        (*colind)[(*rowptr)[col + c_format]] = row + c_format;
        (*rowptr)[col + c_format] ++;
      }

      if( col != row && (row+c_format >= n1 ) && (row+c_format < n2 ) ) {
        (*nzval) [(*rowptr)[row + c_format]] = val;
        (*colind)[(*rowptr)[row + c_format]] = col + c_format;
        (*rowptr)[row + c_format] ++;
      }
   }
   for( i=*n; i>0; i-- ) (*rowptr)[i]    = (*rowptr)[i-1];
   (*rowptr)[0] = 0;

   fclose(fp);

   return 0;
}

