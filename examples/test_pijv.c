
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include "mpi.h"

#include "trlan.h"
#include "trl_map.h"
#include "trl_test_putil.h"

////
// a simple matrix-vector multiplications routine
// defines a diagonal matrix with values (1, 4, 9, 16, 25, ....)
////
typedef struct {
  int    n, frow, nloc, nnz;
  int    *rowptr;
  int    *colind;
  double *nzval;
} csr;

void ijv_pop(const int nrow, const int ncol, const double *xin, const int ldx,
            double *yout, const int ldy, void *mvparam) {
    //
    // ..
    // .. local variables ..
    int i, j, k, n1, n2, iproc, nproc, lcount, *disp, *count;
    double *lxin, *gxin;
    csr *mycsr;
    //
    // ..
    // .. executable statements ..
    MPI_Comm_rank(MPI_COMM_WORLD, &iproc);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    lcount = nrow;
    disp   = malloc( nproc * sizeof(int) );
    count  = malloc( nproc * sizeof(int) );
    MPI_Allgather( &lcount, 1, MPI_INT, count, 1, MPI_INT, MPI_COMM_WORLD );

    disp[0] = 0;
    for( i=0; i<nproc; i++ ) disp[i+1] = disp[i] + count[i];

    lxin = (double*)xin;
    gxin = malloc( (disp[nproc-1]+count[nproc-1]) * sizeof(double) );
    MPI_Allgatherv( lxin, lcount, MPI_DOUBLE, gxin, count, disp, MPI_DOUBLE, MPI_COMM_WORLD );

    n1 = disp[iproc];
    n2 = disp[iproc+1];
    mycsr = (csr*)mvparam;
    for( j=0; j<ncol; j++ )
        for( i=n1; i<n2; i++ ) {
          yout[j*nrow+(i-n1)] = 0.0;
          for( k=mycsr->rowptr[i]; k<mycsr->rowptr[i+1]; k++ )
            yout[j*nrow+(i-n1)] += mycsr->nzval[k] * gxin[j*nrow+mycsr->colind[k]];
        }
        free(gxin);
        free(count);
        free(disp);
}


//// a really simple example of how to use TRLAN
int main( int argn, char **argv ) {
    // This set of parameters tell TRLAN to compute 5 (NED) smallest
    //(LOHI=-1) of a 100 x 100 (NROW) matrix by using a Krylov subspace
    // basis size of 30 (MAXLAN).
    // MEV defines the size of the arrays used to store computed
    // solutions, eval, evec.
    static const int mev=5;
    // local variable declaration
    int iproc, nproc, m, n, nnz, nrow, dn, n1, n2, nloc, lohi, ned, maxlan, restart, lwrk;
    double eval[mev], exact[mev], *evec;
    double *res, *wrk;
    trl_info info;
    int i, j, k, fp, check, nbas, ii, nmis;
    char name2[133], name[150], file[150];
    int tmp1, tmp2, nlen;
    csr *mycsr;
    FILE *ferr, *fin = NULL;
    // initialize info -- tell TRLAN to compute NEV smallest eigenvalues
    // of the diag_op
    if( MPI_Init(&argn,&argv) != MPI_SUCCESS ) {
        printf( "Failed to initialize MPI.\r\n" );
    }
    MPI_Comm_rank(MPI_COMM_WORLD, &iproc);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    mycsr = malloc(sizeof(csr));

    int nx = 0;
    double *exact_evals;
    int maxlan_start = 200;
    int maxlan_end   = 200;
    int maxlan_inc   = 200;
    for (int i=0; i<argn; i++) {
        if (strcmp(argv[i], "--filename") == 0) {
            fin = fopen( argv[++i],"r" );
            //expand_ijv(fin, &n, &m, &nnz, &(mycsr->rowptr), &(mycsr->colind), &(mycsr->nzval) );
            pexpand_ijv(fin, &n, &m, &nnz, &(mycsr->rowptr), &(mycsr->colind), &(mycsr->nzval) );
        }
        if (strcmp(argv[i], "--nx") == 0) {
            nx = atoi( argv[++i] );
            nnz = laplace(NULL, nx, nx, 1, &n, &(mycsr->rowptr), &(mycsr->colind), &(mycsr->nzval), &exact_evals);
            m = n;
        }
        if (strcmp(argv[i], "--maxlan_start") == 0) {
            maxlan_start = atoi( argv[++i] );
        }
        if (strcmp(argv[i], "--maxlan_end") == 0) {
            maxlan_end = atoi( argv[++i] );
        }
        if (strcmp(argv[i], "--maxlan_inc") == 0) {
            maxlan_inc = atoi( argv[++i] );
        }
    }
    if (fin == NULL && nx == 0) {
        printf( " no input file provided\n" );
        return 0;
    }
    dn   = ceil( ((double)n) / nproc );
    n1   = min( n, iproc * dn );
    n2   = min( n, n1 + dn );
    nloc = n2 - n1;
    mycsr->n   = n;
    mycsr->nnz = nnz;
    nrow = nloc;
    evec = malloc( mev*nrow*sizeof(double) );

    ferr=fopen( "error.txt","w" );
    fclose(ferr);
    for( lohi=-1; lohi<=-1; lohi++ ) {
        if (lohi == -1) {
            for (int ii=0; ii<mev; ii++) {
                exact[ii] = exact_evals[ii];
            }
        } else {
            for (int ii=0; ii<mev; ii++) {
                exact[ii] = exact_evals[(n-mev)+ii];
            }
        }
        for( ned=100; ned<=100; ned+=100 ) {
            for( maxlan=maxlan_start; maxlan<=maxlan_end; maxlan+=maxlan_inc ) {
                lwrk=maxlan*(maxlan+10);
                if( lwrk > 0 ) {
                    res = (double*)malloc(lwrk*sizeof(double));
                    wrk = (double*)malloc(lwrk*sizeof(double));
                }
                for( restart=5; restart<=5; restart+=2 ) {
                    trl_init_info( &info, nrow, maxlan, lohi, ned, 1.4901e-8, restart, n, -1 );
                    if( info.my_pe <= 0 ) {
                        ferr=fopen( "error.txt","a" );
                        fprintf( ferr," \n ~~ iteration (ned=%d, maxlan=%d, restart=%d lohi=%d) ~~\n",ned,maxlan,restart,lohi );
                        printf( " \r\n ~~ iteration (ned=%d, maxlan=%d, restart=%d lohi=%d) ~~\r\n",ned,maxlan,restart,lohi );
                        fclose(ferr);
                    }
                    trl_set_iguess( &info, 0, 1, 0, NULL );
                    info.mvparam = mycsr;
                    // the Lanczos recurrence is set to start with [1,1,...,1]^T
                    memset( eval, 0, mev*sizeof(double) );
                    //evec[0]=1.0;
                    //for( i=1; i<nrow; i++ ) {
                    //   evec[i] = 0.0;
                    //}
                    for( i=0; i<nrow; i++ ) {
                        evec[i] = 1.0;
                    }
                    if( sprintf( file,"LOG_Nrow%dNed%dLan%dRes%dLoHi%d",nrow,ned,maxlan,restart,lohi ) < 0 ) {
                        printf( "error writing file name\r\n" );
                    } else {
                        trl_set_debug( &info, 0, file );
                        trlan(ijv_pop, &info, nrow, mev, eval, evec, nrow, lwrk, res );
                        trl_print_info(&info, 3*nrow );
                        if( info.nec > 0 ) {
                            i = info.nec;
                        } else {
                            i = mev;
                        }
                        lwrk = min( lwrk,i+nrow );
                        lwrk = 4*i;
                        if( lohi != 0 ) {
                            trl_check_ritz( ijv_pop, &info, nrow, i, evec, nrow, eval, &check, res, exact, wrk, lwrk );
                        } else {
                            trl_check_ritz( ijv_pop, &info, nrow, i, evec, nrow, eval, &check, res, NULL, wrk, lwrk );
                        }

                        if( info.my_pe <= 0 ) {
                            ferr = fopen( "error.txt","a" );
                            if( info.stat != 0 ) {
                                fprintf( ferr," ** TRLAN FAILED WITH %d **\n **",info.stat );
                            }
                            if( check < 0 ) {
                                fprintf( ferr," -------- ERROR (%d converged) --------\n ",info.nec );
                                printf( " -------- ERROR (%d converged) --------\r\n ",info.nec );
                            } else if( info.nec < info.ned ) {
                                fprintf( ferr," ~~~~~~~~~ only %d converged ~~~~~~~~\n",info.nec );
                                printf( " ~~~~~~~~~ only %d converged ~~~~~~~~\r\n",info.nec );
                            } else {
                                printf( " ********* SUCCESS (%d converged) **********\r\n",info.nec );
                                fprintf( ferr," ********* SUCCESS (%d converged) **********\n",info.nec );
                            }
                            fclose(ferr);
                        }
                        if( info.nec == 0 ) info.nec = min(info.ned, mev-1);
                    }
                }
                if( lwrk > 0 ) {
                    free(res);
                    free(wrk);
                }
            }
        }
    }
    free(mycsr);
    free(evec);
    MPI_Finalize();
    return 0;
}

