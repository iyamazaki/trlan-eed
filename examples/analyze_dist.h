
#ifndef ANALYZE_DIST
#define ANALYZE_DIST
void partition_spmv(int *nrow, int *nnz, st_spmv *spmv);

void analyze_spmv(st_spmv *spmv);

void setup_comm(int proc_id, int num_procs, int m, 
                int *first_row, int *rowptr, int *colind,
                int *num_recv, int **recv_vecs, int *num_send, int **send_vecs);

void setup_recvs(int proc_id, int id, int num_procs, int m,
                 int *first_row, int *rowptr, int *colind,
                 int *num_recv, int **recv_vecs, int *i2p, int *z1);

void setup_sends(int proc_id, int num_procs, int *first_row,
                 int *num_sends, int **send_vecs, int **z1);

void startP2P(double *x, st_spmv *spmv);
void waitP2P(double *x, st_spmv *spmv);
#endif
