#@ job_name        = job
#@ output          = my.out
#@ error           = my.err
#@ job_type        = parallel
#@ environment     = COPY_ALL
#@ notification    = complete
#@ network.MPI     = csss,not_shared,us
#@ node_usage      = not_shared
#@ class           = interactive
#
#@ tasks_per_node  = 8
#@ node            = 1
#@ wall_clock_limit= 00:30:00
#
#@ queue

./pfzsimple

