
#include <stdlib.h>
#include <stdio.h>

#include "mpi.h"
#include "trl_map.h"

#include "trl_test_putil.h"
#include "analyze_dist.h"

void partition_spmv(int *nrow, int *nnz, st_spmv *spmv) {

    int proc_id   = spmv->proc_id;
    int num_procs = spmv->num_procs;

    spmv->first_row = (int*)malloc((num_procs+1) * sizeof(int));
    spmv->count_row = (int*)malloc((num_procs+1) * sizeof(int));

    int p, i, k;
    int frow;
    int mloc = (spmv->M + num_procs-1)/ num_procs;
    spmv->first_row[0] = 0;
    for (p=1; p<=num_procs; p++) {
        spmv->first_row[p] = min(p*mloc, spmv->M);
        spmv->count_row[p-1] = spmv->first_row[p] - spmv->first_row[p-1];
        //if (proc_id == 0) printf( " first_row[%d] = %d, count_row[%d] = %d\n",p,  spmv->first_row[p],
        //                                                                      p-1,spmv->count_row[p-1] );
    }

    frow = spmv->first_row[proc_id];
    mloc = spmv->count_row[proc_id];
    *nnz  = spmv->RowPtr[frow+mloc] - spmv->RowPtr[frow];

    spmv->rowptr = (int*)   malloc((1+mloc) * sizeof(int));
    spmv->colind = (int*)   malloc( *nnz    * sizeof(int));
    spmv->nzval  = (double*)malloc( *nnz    * sizeof(double));

    *nnz = 0;
    spmv->rowptr[0] = 0;
    for (i=frow; i<frow+mloc; i++) {
        for (k=spmv->RowPtr[i]; k<spmv->RowPtr[i+1]; k++) {
            spmv->colind[*nnz] = spmv->ColInd[k];
            spmv->nzval[*nnz]  = spmv->NzVal[k];

            (*nnz) ++;
        }
        spmv->rowptr[i-frow+1] = *nnz;
    }

    spmv->m = mloc;
    *nrow = mloc;
}

void analyze_spmv(st_spmv *spmv) {
    if (spmv->num_procs > 1) {
        /* allocate */
        spmv->send_req = (MPI_Request*)malloc(spmv->num_procs * sizeof(MPI_Request));
        spmv->recv_req = (MPI_Request*)malloc(spmv->num_procs * sizeof(MPI_Request));
        // recv_vecs[p]: non-local elements to receive from p
        spmv->num_recvs = (int*) malloc( spmv->num_procs * sizeof(int) );
        spmv->recv_vecs = (int**)malloc( spmv->num_procs * sizeof(int*) );
        // send_vecs[p]: local elements to send to p
        spmv->num_sends = (int*) malloc( spmv->num_procs * sizeof(int) );
        spmv->send_vecs = (int**)malloc( spmv->num_procs * sizeof(int*) );

        setup_comm(spmv->proc_id, spmv->num_procs, spmv->M,
                   spmv->first_row, spmv->RowPtr, spmv->ColInd,
                   spmv->num_recvs, spmv->recv_vecs,
                   spmv->num_sends, spmv->send_vecs);

        /* allocate communication buffer */
        int p;
        int recv_buffer_size = 0;
        int send_buffer_size = 0;
        for (p=0; p<spmv->num_procs; p++) {
            if (p != spmv->proc_id) {
                recv_buffer_size += spmv->num_recvs[p];
                send_buffer_size += spmv->num_sends[p];
            }
        }
        spmv->send_buffer = (double*)malloc(send_buffer_size * sizeof(double));
        spmv->recv_buffer = (double*)malloc(recv_buffer_size * sizeof(double));
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
void setup_comm(int proc_id, int num_procs, int m,
                int *first_row, int *rowptr, int *colind,
                int *num_recvs, int **recv_vecs, int *num_sends, int **send_vecs) {

    int p, i;
    int **z1 = (int**)malloc(num_procs * sizeof(int*));
    int *i2p = (int*)malloc(m * sizeof(int));
    for (p=0; p<num_procs; p++) {
        for(i=first_row[p]; i<first_row[p+1]; i++) i2p[i] = p;
    }

    for (p=0; p<num_procs; p++){
        z1[p] = (int*)malloc(m * sizeof(int));
        setup_recvs(proc_id, p, num_procs, m,
                    first_row, rowptr, colind,
                    num_recvs, recv_vecs, i2p, z1[p]);
    }
    setup_sends(proc_id, num_procs, first_row,
                num_sends, send_vecs, z1);

    free(i2p);
    for (p=0; p<num_procs; p++){
        free(z1[p]);
    }
    free(z1);
}


void setup_recvs(int proc_id, int id, int num_procs, int m, 
                 int *first_row, int *rowptr, int *colind,
                 int *num_recvs, int **recv_vecs, int *i2p, int *z1) {
    int start = first_row[id];
    int end = first_row[id+1];

    int i, j;
    for (i=0; i<m; i++) z1[i] = -1;
    for (i=start; i<end; i++) z1[i] = 0;
    for (j=start; j<end; j++) {
        for (i=rowptr[j]; i<rowptr[j+1]; i++) {
            if (z1[colind[i]] == -1) {
                z1[colind[i]] = 1;
            }
        }
    }

    if (id == proc_id) {
        // determine the rows of A needed in local matrix B from procs
        int p;
        for (p=0; p<num_procs; p++) num_recvs[p] = 0;
        for(i=0; i<start; i++) {
            if (z1[i] != -1) num_recvs[i2p[i]]++;
        }
        for(i=end; i<m; i++) {
            if (z1[i] != -1) num_recvs[i2p[i]]++;
        }
        num_recvs[id] = 0;
        for (p=0; p<num_procs; p++) {
            if (p != proc_id && num_recvs[p] > 0) {
                recv_vecs[p] = (int*)malloc(num_recvs[p] * sizeof(int));
                num_recvs[p] = 0;
                for (i=first_row[p]; i<first_row[p+1]; i++) {
                    if (z1[i] != -1) {
                        recv_vecs[p][num_recvs[p]] = i;
                        num_recvs[p] ++;
                        num_recvs[id]++;
                    }
                }
            } else {
                recv_vecs[p] = NULL;
            }
        }
    }
}

void setup_sends(int proc_id, int num_procs, int *first_row, 
                 int *num_sends, int **send_vecs, int **z1) {

    int p, j;
    num_sends[proc_id] = 0;
    for (p=0; p<num_procs; p++) {
        if (proc_id != p) {
            num_sends[p] = 0;
            for (j=first_row[proc_id]; j<first_row[proc_id+1]; j++) {
                if (z1[p][j] != -1) {
                    num_sends[p] ++;
                    num_sends[proc_id] ++;
                }
            }
            if (num_sends[p] > 0) {
                send_vecs[p] = (int*)malloc(num_sends[p] * sizeof(int));
                num_sends[p] = 0;
                for (j=first_row[proc_id]; j<first_row[proc_id+1]; j++) {
                    if (z1[p][j] != -1) {
                        send_vecs[p][num_sends[p]] = j;
                        num_sends[p] ++;
                    }
                }
            } else {
                send_vecs[p] = NULL;
            }
        } else {
            send_vecs[p] = NULL;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
void startP2P(double *x, st_spmv *spmv) {
    int p, i;
    int send = 0, recv = 0;
    for (p=0; p<spmv->num_procs; p++) {
        if (p != spmv->proc_id) {
            int count = spmv->num_sends[p];
            if (count > 0) {
                for (i=0; i<count; i++) {
                    spmv->send_buffer[send+i] = x[spmv->send_vecs[p][i]];
                }
                MPI_Isend(&(spmv->send_buffer[send]), count, MPI_DOUBLE, p, 0, MPI_COMM_WORLD, &(spmv->send_req[p]));
                send += count;
            }

            count = spmv->num_recvs[p];
            if (count > 0) {
                MPI_Irecv(&(spmv->recv_buffer[recv]), count, MPI_DOUBLE, p, 0, MPI_COMM_WORLD, &(spmv->recv_req[p]));
                recv += count;
            }
        }
     }
}

void waitP2P(double *x, st_spmv *spmv) {
    int p, i, recv = 0;
    MPI_Status status;
    for (p=0; p<spmv->num_procs; p++) {
        if (p != spmv->proc_id) {
            if (spmv->num_sends[p] > 0) {
                MPI_Wait(&(spmv->send_req[p]), &status);
            }
            int count = spmv->num_recvs[p];
            if (count > 0) {
                MPI_Wait(&(spmv->recv_req[p]), &status);
                for (i=0; i<count; i++) {
                    x[spmv->recv_vecs[p][i]] = spmv->recv_buffer[recv+i];
                }
                recv += count;
            }
        }
    }
}
