############################################################
# nuTRLan make include file
############################################################
#
# C compiler
  FC      = ftn
  CC      = cc
  MPICC   = cc
  LOADER  = cc
  PLOADER = cc
#
# Debuging
  DEBUG = 
  OPT = -DBLAS
#  OPT = -g -DBLAS
#  If linking to CLAPACK
#  OPT = -O3
#
# Make command
  MAKE = make
#  MAKEFLAGS = TOP=${PWD}
#
# nuTRLan home directory
#  TOP = ${PWD}
  NUTRLAN = $(TOP)/libnutrlan.a
#
# External directories
#  DIR_CLAPACK = /root/ichi/ricpack-1.0/CLAPACK
#
# External libraries
  LIB_DIR = 
  MATH    = 
  BLAS    =
  LAPACK  = 
#  SLU_DIR = /global/u2/y/yamazaki/franklin_Jan21.2010/original/SuperLU_DIST_2.3
#  SLU_LIB = $(SLU_DIR)/lib/libsuperlu_dist_2.3.a
#  SLU_INC = -I$(SLU_DIR)/SRC
#  METIS_LIB = $(METIS)
#  CDF_INC = -I/global/homes/l/llee/Software/parallel-netcdf-1.1.1/src/lib/
#  CDF_LIB = /global/homes/l/llee/Software/parallel-netcdf-1.1.1/src/lib/libpnetcdf.a

#  CLAPACK = $(DIR_CLAPACK)/lapack_IA64.a
#  F2C     = -lF77 -lI77
  LIB     = $(LIB_DIR) $(LAPACK) $(BLAS) $(CLAPACK) $(F2C) $(MATH)
  PLIB    = $(LIB)
#
# Include files
#  INC_CLAPACK = -I$(DIR_CLAPACK)/F2CLIBS \
#                -I$(DIR_CLAPACK)/BLAS/WRAP
  INC_NUTRLAN  = -I$(TOP)/INC
  INC         = $(INC_CLAPACK) $(INC_NUTRLAN)
  FINC        = -I$(TOP)/FORTRAN
#
# Object files
  OBJ_REAL = dsort2.o dstqrb.o trlan.o trlaux.o trlcore.o restart.o trlmap.o
  OBJ_CPLX = zdgemm.o zdgemv.o zdaxpy.o ztrlan.o ztrlaux.o ztrlcore.o
  OBJ = $(OBJ_REAL) $(OBJ_CPLX)
  OBJ_SERIAL = $(OBJ) trl_comm_none.o ztrl_comm_none.o
  OBJ_PARALLEL = $(OBJ) trl_comm_mpi.o ztrl_comm_mpi.o
  FOBJ = ftrlan.o
