
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <math.h>
#include <float.h>
#include <mpi.h>

#include "trlan.h"
#include "trl_map.h"
#include "trlcore_i.h"
#include "trlaux_i.h"
#include "catrlan.h"


/* for version 1 */
#define CA_REORTHO
#define CA_UPDATE_R

#define CA_REORTHO_PREV
#define CA_REORTHO_3TERM

/* orthogonalization options */
#define CATRLan_CHOL
//#define CATRLan_MGS
//#define CATRLan_SLA
//#define CATRLan_CAQR


/*
// ------------------------------------------ //
// reorder Ritz values based on Leja ordering //
// ------------------------------------------ //
*/
void catrl_order_shifts( int mloc, int s, double *shift ) {
    #ifdef NEWTON_BASIS
    int i, j, k, vali;
    double val, cap, max_cap, scal;
    if (shift == NULL || s < 1) return;

    /* Leja ordering */
    // the largest one, first
    val = shift[0];
    shift[0] = shift[mloc-1];
    shift[mloc-1] = val;

    scal = fabs( shift[0] ); // an attempt to avoid over-flow 
    for( i=1; i<s; i++ ) {
        // picking the next best
        max_cap = 0.0;
        for( j=i; j<mloc; j++ ) {
            //computing the capacity
            cap = 1.0;
            for( k=0; k<i; k ++ ) cap *= fabs( shift[j] - shift[k] ) / scal;
            if( cap > max_cap ) {
                max_cap = cap;
                vali = j;
            }
        }
        val = shift[i];
        shift[i] = shift[vali];
        shift[vali] = val;
    }
    #endif
}

/*
// -------------------------------------------------- //
// compute matrix powers by simply calling op s times //
// -------------------------------------------------- //
*/
void catrl_matrix_kernel( int nrow, int mks, double *W, trl_matvec op, 
                          double *shift, void *mvparam ) {

    int i, k, i__1 = 1;
    for( k=0; k<mks; k++ ) {
        #ifdef TRL_FORTRAN_COMPATIBLE
        op(&nrow, &i__1, &W[k*nrow], &nrow, &W[(k+1)*nrow], &nrow);
        #else
        op(nrow, i__1, &W[k*nrow], nrow, &W[(k+1)*nrow], nrow, mvparam);
        #endif

        #ifdef NEWTON_BASIS
        for( i=0; i<nrow; i++ ) {
            W[(k+1)*nrow+i] -= shift[k]*W[k*nrow+i];
        }
        #endif
    }
}


/*
// ------------------------------------------------------------------------- //
// orthogonalize the vectors against the previous vectors based on block CGS //
// ------------------------------------------------------------------------- //
*/
void catrl_cgs( int nrow, int mV, double *V, int mW, double *W, int ldw, double *R, double *lR, 
                trl_info *info ) {
    /* "re"orthogonalization to the previous sets of vectors */

    char trans = 'T', notrans = 'N';
    int i, j, k, one_i = 1;
    double mone = -1.0, one = 1.0, zero = 0.0, beta;

    /* full "re"-orthogonalizing against the previous vectors *
     * lR = V' * W                                            */
    trl_dgemm(&trans, &notrans, mV, mW, nrow, one, V, nrow, W, ldw,
              zero, lR, mV);
    if (info->mpicom == -INT_MAX) {
        trl_dcopy(mV*mW, lR, one_i, R, one_i);
    } else {
        MPI_Allreduce( lR, R, mV*mW, MPI_DOUBLE, MPI_SUM, info->mpicom ); 
    }
    trl_dgemm(&notrans, &notrans, nrow, mW, mV, mone, V, nrow, R, mV,
              one, W, ldw);

    #ifdef  CA_REORTHO_3TERM
    /* orthogonalize */
    /* int i1 = 2, i2 = min( i1, mW ); */
    int i1 = mW, i2 = mW;
    trl_dgemm(&trans, &notrans, i1, i2, nrow, one, &V[(mV-i1)*nrow], nrow, W, ldw,
              zero, lR, i1);
    if (info->mpicom == -INT_MAX) {
        trl_dcopy(i1*i2, lR, one_i, R, one_i);
    } else {
        MPI_Allreduce( lR, R, i1*i2, MPI_DOUBLE, MPI_SUM, info->mpicom );
    }
    trl_dgemm(&notrans, &notrans, nrow, i2, i1, mone, &V[(mV-i1)*nrow], nrow, R, i1,
              one, W, ldw);
    #endif

    /* i thought we don't need to full "re"-orthogonalize twice, but.. */
    #ifdef  CA_REORTHO_PREV  
    /* reorthogonalized                                                        *
     * note: R is over-written since it is not needed (see catrl_update_proj)  */
    trl_dgemm(&trans, &notrans, mV, mW, nrow, one, V, nrow, W, ldw,
              zero, lR, mV);
    if (info->mpicom == -INT_MAX) {
        trl_dcopy(mV*mW, lR, one_i, R, one_i);
    } else {
        MPI_Allreduce( lR, R, mV*mW, MPI_DOUBLE, MPI_SUM, info->mpicom ); 
    }
    trl_dgemm(&notrans, &notrans, nrow, mW, mV, mone, V, nrow, R, mV,
              one, W, ldw);
    #endif  
}

/*
// ------------------------------------------------------- //
// orthogonalize the vectors among themselves based on CGS //
// ------------------------------------------------------- //
*/
void catrl_qr( int nrow, int m, double *W, double *lR, double *R, double *T, trl_info *info ) {
    /* orthogonalization within the current window using gram schmidt */

    char trans = 'T', notrans = 'N';
    int i, j, k, one_i = 1;
    double mone = -1.0, one = 1.0, zero = 0.0, beta;

    /* the first vector has been already normalized */
    /* for( i=0; i<(m+1)*(m+1); i++ ) R[i] = zero;  */
    R[0] = one;
    T[0] = one;
    for( j=1; j<=m; j++ ) {
        /* orthogonalizing against the previous vectors */
        trl_dgemv(&trans, nrow, j, one, W, nrow, &W[j*nrow], one_i,
                  zero, lR, one_i);
        if (info->mpicom == -INT_MAX) {
            trl_dcopy(j, lR, one_i, &R[j*(m+1)], one_i);
        } else {
            MPI_Allreduce( lR, &R[j*(m+1)], j, MPI_DOUBLE, MPI_SUM, info->mpicom ); 
        }
        trl_dgemv(&notrans, nrow, j, mone, W, nrow, &R[j*(m+1)], one_i,
                  one, &W[j*nrow], one_i);

        /* normalizing the current vector */
        lR[0] = trl_ddot(nrow, &W[j*nrow], one_i, &W[j*nrow], one_i );
        if (info->mpicom == -INT_MAX) {
            trl_dcopy(1, lR, one_i, &R[j*(m+1)+j], one_i);
        } else {
            MPI_Allreduce( lR, &R[j*(m+1)+j], 1, MPI_DOUBLE, MPI_SUM, info->mpicom );
        }
        R[j*(m+1)+j] = sqrt(R[j*(m+1)+j]);
        beta = one / R[j*(m+1)+j];
        trl_dscal( nrow, beta, &W[j*nrow], one_i );

        #ifdef CA_REORTHO
        /* reorthogonalize for numerical stability */
        trl_dgemv(&trans, nrow, j, one, W, nrow, &W[j*nrow], one_i,
                  zero, lR, one_i);
        #ifdef CA_UPDATE_R
        if (info->mpicom == -INT_MAX) {
            trl_dcopy(j, lR, one_i, &T[j*(m+1)], one_i);
        } else {
            MPI_Allreduce( lR, &T[j*(m+1)], j, MPI_DOUBLE, MPI_SUM, info->mpicom ); 
        }
        trl_dgemv(&notrans, nrow, j, mone, W, nrow, &T[j*(m+1)], one_i,
                  one, &W[j*nrow], one_i);

        /* normalizing the current vector */
        lR[0] = trl_ddot(nrow, &W[j*nrow], one_i, &W[j*nrow], one_i );
        if (info->mpicom == -INT_MAX) {
            trl_dcopy(1, lR, one_i, &T[j*(m+1)+j], one_i);
        } else {
            MPI_Allreduce( lR, &T[j*(m+1)+j], 1, MPI_DOUBLE, MPI_SUM, info->mpicom );
        }

        T[j*(m+1)+j] = sqrt(T[j*(m+1)+j]);
        beta = one / T[j*(m+1)+j];
        trl_dscal( nrow, beta, &W[j*nrow], one_i );
        #else
        if (info->mpicom == -INT_MAX) {
            trl_dcopy(j, lR, one_i, T, one_i);
        } else {
            MPI_Allreduce( lR, T, j, MPI_DOUBLE, MPI_SUM, info->mpicom ); 
        }
        trl_dgemv(&notrans, nrow, j, mone, W, nrow, T, one_i,
                  one, &W[j*nrow], one_i);
        #endif
        #endif
    }

    #ifdef CA_UPDATE_R
    for( j=1; j<=m; j++ ) {
        R[j*(m+1)+j-1] = T[(j-1)*(m+1)+j-1] * R[j*(m+1)+j-1] + T[j*(m+1)+j-1] * R[j*(m+1)+j];
    }
    for( j=0; j<=m; j++ ) {
        R[j*(m+1)+j] *= T[j*(m+1)+j];
    }
    #endif
}

/*
// ------------------------------------------------- //
// update the projected symmetric tridiagonal matrix //
// ------------------------------------------------- //
*/
void catrl_update_proj2( int ncol, double *R, double *shift, double *alpha, double *beta ) {
    int i;

    #ifdef NEWTON_BASIS
    alpha[0] = R[1+ncol] + shift[0]*R[0];
    #else
    alpha[0] = R[1+ncol];
    #endif
    for( i=1; i<ncol; i++ ) {
        // apply lower-triangular solution with R2' on (R*B)'
        #ifdef NEWTON_BASIS
        //beta [i-1] = (R[i*(1+ncol)+i]+shift[i]*R[(i-1)*(1+ncol)+i])/R[(i-1)*(1+ncol)+i-1];
        beta [i-1] = R[i*(1+ncol)+i]/R[(i-1)*(1+ncol)+i-1];
        alpha[i]   = R[(i+1)*(1+ncol)+i]+shift[i]*R[i*(1+ncol)+i];
        #else
        beta [i-1] = R[i*(1+ncol)+i]/R[(i-1)*(1+ncol)+i-1];
        alpha[i]   = R[(i+1)*(1+ncol)+i];
        #endif
        alpha[i]  -= R[i*(1+ncol)+i-1] * beta[i-1];
        alpha[i]  /= R[i*(1+ncol)+i];

        //printf( "beta[%d]=%e\n",i-1,beta[i-1] );
        //printf( "alpha[%d]=%e\n",i,alpha[i] );
    }
    beta[ncol-1] = R[ncol*(1+ncol)+ncol]/R[(ncol-1)*(1+ncol)+ncol-1];
    //printf( "beta[%d]=%e\n",ncol-1,beta[ncol-1] );
}


/*
// ------------------------------------------------------ //
// orthogonalize new vectors against previous vectors and //
// among themselves                                       //
// ------------------------------------------------------ //
*/
int catrl_3term( int itrs, int nrow, int mV, int m, double *V, int ldv, double *W, int ldw, double *lR0, double *R0,
                 double *lR, double *R, double *T, trl_info *info, double *shift ) {
    /* orthogonalization within the current window using gram schmidt */

    char trans = 'T', notrans = 'N';
    int i, j, k, pm, one_i = 1, flag = 0, flag2 = 0;
    double mone = -1.0, one = 1.0, zero = 0.0, *pV, kappa, beta, new_norm, old_norm;
    #ifdef __CLK_CLOCK
    clock_t clk1, clk2;
    #else
    int clk1, clk2;
    #endif
    kappa = DBL_EPSILON/0.83;
    kappa = 0.5;

    /* the initial vector has been normalized */
    R[0] = one; 
    #define BLOCK_3TERM
    #ifdef  BLOCK_3TERM
    if( mV > m && itrs > 0 ) {
        pm = m+1;
        pV = &(V[(mV-pm)*ldv]);
    } else {
        pm = mV;
        pV = V;
    }
    #else
    pm = mV;
    pV = V;
    #endif

    /* the vectors are first orthogonalized against all the previous */
    trl_dgemm(&trans, &notrans, pm, m, nrow, one, pV, ldv, &W[ldw], ldw,
              zero, lR0, pm);
    if (info->mpicom == -INT_MAX) {
        trl_dcopy(pm*m, lR0, one_i, R0, one_i);
    } else {
        MPI_Allreduce( lR0, R0, pm*m, MPI_DOUBLE, MPI_SUM, info->mpicom );
    }
    trl_dgemm(&notrans, &notrans, nrow, m, pm, mone, pV, ldv, R0, pm,
              one, &W[ldw], ldw);
    for( j=1; j<=m; j++ ) {
        R[j*(m+1)] = R0[j*pm-1];
    }
#if defined(BLOCK_3TERM_REORTHO)
    trl_dgemm(&trans, &notrans, pm, m, nrow, one, pV, ldv, &W[ldw], ldw,
              zero, lR0, pm);
    if (info->mpicom == -INT_MAX) {
        trl_dcopy(pm*m, lR0, one_i, R0, one_i);
    } else {
        MPI_Allreduce( lR0, R0, pm*m, MPI_DOUBLE, MPI_SUM, info->mpicom );
    }
    trl_dgemm(&notrans, &notrans, nrow, m, pm, mone, pV, ldv, R0, pm,
              one, &W[ldw], ldw);
    for( j=1; j<=m; j++ ) {
        R[j*(m+1)] += R0[j*pm-1];
    }
#endif


    /* normalizing the first vector */
    lR[0] = trl_ddot(nrow, &W[ldw], one_i, &W[ldw], one_i );
    if (info->mpicom == -INT_MAX) {
        trl_dcopy(1, lR, one_i, &R[m+2], one_i);
    } else {
        MPI_Allreduce( lR, &R[m+2], 1, MPI_DOUBLE, MPI_SUM, info->mpicom );
    }
    new_norm = old_norm = R[m+2];
    R[m+2] = sqrt(R[m+2]);
    beta = one / R[m+2];
    trl_dscal( nrow, beta, &W[ldw], one_i );

    //#define CHECK_REORTH
    #ifdef  CHECK_REORTH
    flag = 0;
    for( i=1; i<=mV; i++ ) { 
        old_norm += R0[mV-i]*R0[mV-i];
        if( sqrt(new_norm / old_norm) < kappa ) {
            flag = 1;
            i = mV+1;
        }
        new_norm = old_norm;
    }
    #endif

    /* orthogonalized among themselves */
    clk1 = trl_clk();
    #ifdef  CATRLan_MGS
    for( j=2; j<=m; j++ ) {
        /* orthogonalizing against the previous vectors */
        trl_dgemv(&trans, nrow, j-1, one, &W[ldw], ldw, &W[j*ldw], one_i,
                  zero, lR, one_i);
        MPI_Allreduce( lR, &R[j*(m+1)+1], j-1, MPI_DOUBLE, MPI_SUM, info->mpicom ); 
        trl_dgemv(&notrans, nrow, j-1, mone, &W[ldw], ldw, &R[j*(m+1)+1], one_i,
                  one, &W[j*ldw], one_i);

        /* normalizing the current vector */
        lR[0] = trl_ddot(nrow, &W[j*ldw], one_i, &W[j*ldw], one_i );
        MPI_Allreduce( lR, &R[j*(m+1)+j], 1, MPI_DOUBLE, MPI_SUM, info->mpicom );
        old_norm = new_norm = R[j*(m+1)+j];
        R[j*(m+1)+j] = sqrt(R[j*(m+1)+j]);
        beta = one / R[j*(m+1)+j];
        trl_dscal( nrow, beta, &W[j*ldw], one_i );
    }
    #elif defined (CATRLan_SLA)
    catrl_qr3_( &(info->blacs_ictxt), &nrow,
                &(info->ntot), &m,
                &(info->blacs_nprow), &(info->blacs_npcol),
                &(info->blacs_myrow), &(info->blacs_mycol),
                &W[ldw], lR, T, &m );

    for( j=2; j<=m; j++ ) {
        for( i=0; i<j; i++ ) R[j*(m+1)+i+1]=T[(j-1)*m+i];
    }
    #elif defined (CATRLan_CAQR)
    reducehouseholder_A_v2(nrow, m, &W[ldw], ldw, T, m, info->mpicom);
    for( i=0; i<m; i++ ) {
        if(T[i*m+i]<0.0) {
            for( j=i; j<m; j++ ) T[j*m+i] = -T[j*m+i];
            for( j=0; j<nrow; j++ ) W[(i+1)*ldw+j] = -W[(i+1)*ldw+j];
        }
    }
    for( j=2; j<=m; j++ ) {
        for( i=0; i<j; i++ ) R[j*(m+1)+i+1]=T[(j-1)*m+i];
    }
    #else
    int mp1 = m+1, mm1 = m-1;
    char upper = 'U', right = 'R', no = 'N';
    // form W'*W
    trl_dgemm(&trans, &notrans, m, m, nrow, one, &W[ldw], ldw, &W[ldw], ldw, zero, lR, m );
    if (info->mpicom == -INT_MAX) {
        trl_dcopy(m*m, lR, one_i, T, one_i);
    } else {
        MPI_Allreduce( lR, T, m*m, MPI_DOUBLE, MPI_SUM, info->mpicom ); 
    }

    // compute the cholesky, R
    int iinfo = trl_dpotrf( &upper, m, T, m );
    if (iinfo != 0) printf( " potrf failed with info=%d\n",iinfo );

    // compute Q
    trl_dtrsm(&right, &upper, &notrans, &no, nrow, m, one, T, m, &W[ldw], ldw);

    for( j=2; j<=m; j++ ) {
        for( i=0; i<j; i++ ) R[j*(m+1)+i+1]=T[(j-1)*m+i];
    }
    #endif

    #ifndef CHECK_REORTH
    add_clock_ticks(info, &(info->clk_qr), &(info->tick_q), clk1);
    #else
    for( i=1; i<=mV; i++ ) { 
        old_norm += R0[j*mV-i]*R0[j*mV-i];
        if( sqrt(new_norm / old_norm) < kappa ) {
            flag = 1;
        }
        new_norm = old_norm;
    }

    for( i=1; i<j; i++ ) {
        old_norm += R[j*(m+1)+i]*R[j*(m+1)+i];
        if( sqrt(new_norm / old_norm) < kappa ) {
            #if CATRLan_MGS
            /* orthogonalizing against the previous vectors */
            trl_dgemv(&trans, nrow, j-1, one, &W[ldw], ldw, &W[j*ldw], one_i,
                      zero, lR, one_i);
            MPI_Allreduce( lR, &T[j*(m+1)+1], j-1, MPI_DOUBLE, MPI_SUM, info->mpicom ); 
            trl_dgemv(&notrans, nrow, j-1, mone, &W[ldw], ldw, &T[j*(m+1)+1], one_i,
                      one, &W[j*ldw], one_i);
            #endif
            info->north ++;
            flag2 = 1;
            i = j;
        }
        new_norm = old_norm;
    }
    add_clock_ticks(info, &(info->clk_qr), &(info->tick_q), clk1);

    if( flag == 1 ) {
        /* the vectors are first orthogonalized against all the previous */
        trl_dgemm(&trans, &notrans, mV, m, nrow, one, V, ldv, &W[ldw], ldw,
                  zero, lR0, mV);
        MPI_Allreduce( lR0, R0, mV*m, MPI_DOUBLE, MPI_SUM, info->mpicom );
        trl_dgemm(&notrans, &notrans, nrow, m, mV, mone, V, ldv, R0, mV,
                  one, &W[ldw], ldw);
        for( j=1; j<=m; j++ ) {
            T[j*(m+1)] = R0[j*mV-1];
        }
    }

    #ifdef CATRLan_CHOL
    clk1 = trl_clk();
    if( flag2 == 1 ) {
        int mp1 = m+1, mm1 = m-1;
        char upper = 'U', right = 'R', no = 'N';
        // form W'*W
        trl_dgemm(&trans, &notrans, m, m, nrow, one, &W[ldw], ldw, &W[ldw], ldw, zero, lR, m );
        MPI_Allreduce( lR, T, m*m, MPI_DOUBLE, MPI_SUM, info->mpicom ); 

        // compute the cholesky, R
        int iinfo = trl_dpotrf( &upper, m, T, m );
        if (iinfo != 0) printf( " potrf failed with info=%d\n",iinfo );

        // compute Q
        trl_dtrsm(&right, &upper, &notrans, &no, nrow, m, one, T, m, &W[ldw], ldw);
    }
    add_clock_ticks(info, &(info->clk_qr), &(info->tick_q), clk1);
    #endif

    if( flag == 1 || flag2 == 1 ) {
        for( j=1; j<=m; j++ ) {
            /* normalizing the current vector */
            lR[0] = trl_ddot(nrow, &W[j*ldw], one_i, &W[j*ldw], one_i );
            MPI_Allreduce( lR, &T[j*(m+1)+j], 1, MPI_DOUBLE, MPI_SUM, info->mpicom );
            T[j*(m+1)+j] = sqrt(T[j*(m+1)+j]);
            beta = one / T[j*(m+1)+j];
            trl_dscal( nrow, beta, &W[ldw], one_i );
        }
    }
    #endif /* end of CHECK_REORTH */

    #ifdef CHECK_REORTH
    return 0;
    #else
    return 1;
    #endif
}

/*
// --------------------------------------------------------- //
// re-orthogonalize new vectors against previous vectors and //
// among themselves                                          //
// --------------------------------------------------------- //
*/
void catrl_reortho( int nrow, int mV, int m, double *V, int ldv, double *W, int ldw, double *lR0, double *R0,
                    double *lR, double *R, double *T, trl_info *info ) {
    /* "re"orthogonalization to the previous sets of vectors */

    char trans = 'T', notrans = 'N';
    int i, j, k, one_i = 1;
    double mone = -1.0, one = 1.0, zero = 0.0, beta;
    #ifdef __CLK_CLOCK
    clock_t clk1, clk2;
    #else
    int clk1, clk2;
    #endif

    /* orthogonalizing against the previous vectors *
     * lR = V' * W                                  */
    trl_dgemm(&trans, &notrans, mV, m, nrow, one, V, ldv, &W[ldw], ldw,
              zero, lR0, mV);
    if (info->mpicom == -INT_MAX) {
        trl_dcopy(mV*m, lR0, one_i, R0, one_i);
    } else {
        MPI_Allreduce( lR0, R0, mV*m, MPI_DOUBLE, MPI_SUM, info->mpicom );
    }
    trl_dgemm(&notrans, &notrans, nrow, m, mV, mone, V, ldv, R0, mV,
              one, &W[ldw], ldw);
    for( j=1; j<=m; j++ ) {
        T[j*(m+1)] = R0[j*mV-1];
    }

    /* normalizing the first vector */
    lR[0] = trl_ddot(nrow, &W[ldw], one_i, &W[ldw], one_i );
    if (info->mpicom == -INT_MAX) {
        trl_dcopy(1, lR, one_i, &T[m+2], one_i);
    } else {
        MPI_Allreduce( lR, &T[m+2], 1, MPI_DOUBLE, MPI_SUM, info->mpicom );
    }
    beta = one / T[m+2];
    trl_dscal( nrow, beta, &W[ldw], one_i );

    /* orthogonalized among themselves */
    clk1 = trl_clk();
    T[0] = one;
    #ifdef  CATRLan_CHOL
    int mp1 = m+1, mm1 = m-1;
    char upper = 'U', right = 'R', no = 'N';
    // form W'*W
    trl_dgemm(&trans, &notrans, m, m, nrow, one, &W[ldw], ldw, &W[ldw], ldw, zero, lR, m );
    if (info->mpicom == -INT_MAX) {
        trl_dcopy(m*m, lR, one_i, R0, one_i);
    } else {
        MPI_Allreduce( lR, R0, m*m, MPI_DOUBLE, MPI_SUM, info->mpicom );
    }

    // compute the cholesky, R
    int iinfo = trl_dpotrf( &upper, m, R0, m );
    if (iinfo != 0) printf( " reortho:potrf failed with info=%d\n",iinfo );

    // compute Q
    trl_dtrsm(&right, &upper, &notrans, &no, nrow, m, one, R0, m, &W[ldw], ldw);

    // copy the R-update
    for( j=2; j<=m; j++ ) {
        for( i=0; i<j; i++ ) T[j*(m+1)+i+1] = R0[(j-1)*m+i];
        for( i=j; i<m; i++ ) T[j*(m+1)+i+1] = 0.0;
    }
    #ifdef CHOLQR2
    // form W'*W
    memset(lR, 0, m*m*sizeof(double));
    trl_dgemm(&trans, &notrans, m, m, nrow, one, &W[ldw], ldw, &W[ldw], ldw, zero, lR, m );
    if (info->mpicom == -INT_MAX) {
        trl_dcopy(m*m, lR, one_i, R0, one_i);
    } else {
        MPI_Allreduce( lR, R0, m*m, MPI_DOUBLE, MPI_SUM, info->mpicom );
    }

    // compute the cholesky, R
    iinfo = trl_dpotrf( &upper, m, R0, m );
    if (iinfo != 0) printf( " re-reortho:potrf failed with info=%d\n",iinfo );

    // compute Q
    trl_dtrsm(&right, &upper, &notrans, &no, nrow, m, one, R0, m, &W[ldw], ldw);

    // update R
    int ldt = m+1;
    dtrmm_("L","U","N","N", &m,&m, &one,R0,&m, &T[1+ldt], &ldt);
    info->north += m;
    #endif

    #elif defined (CATRLan_SLA)
    int mksp1 = m+1;
    catrl_qr3_( &(info->blacs_ictxt), &nrow,
                &(info->ntot), &m,
                &(info->blacs_nprow), &(info->blacs_npcol),
                &(info->blacs_myrow), &(info->blacs_mycol),
                &W[ldw], lR, &T[mksp1+1], &mksp1 );
    #elif defined (CATRLan_CAQR)
    reducehouseholder_A_v2(nrow, m, &W[ldw], ldw, R0, m, info->mpicom);
    for( i=0; i<m; i++ ) {
        if(R0[i*m+i]<0.0) {
            for( j=i; j<m; j++ ) R0[j*m+i] = -R0[j*m+i];
            for( j=0; j<nrow; j++ ) W[(i+1)*ldw+j] = -W[(i+1)*ldw+j];
        }
    }
    for( j=2; j<=m; j++ ) {
        for( i=0; i<j; i++ ) T[j*(m+1)+i+1]=R0[(j-1)*m+i];
    }
    #else
    for( j=2; j<=m; j++ ) {
        /* reorthogonalize against previous vectors */
        trl_dgemv(&trans, nrow, j-1, one, &W[ldw], ldw, &W[j*ldw], one_i,
                  zero, lR, one_i);
        if (info->mpicom == -INT_MAX) {
            trl_dcopy(j-1, lR, one_i, &T[j*(m+1)+1], one_i);
        } else {
            MPI_Allreduce( lR, &T[j*(m+1)+1], j-1, MPI_DOUBLE, MPI_SUM, info->mpicom );
        }
        trl_dgemv(&notrans, nrow, j-1, mone, &W[ldw], ldw, &T[j*(m+1)+1], one_i,
                  one, &W[j*ldw], one_i);

        /* normalizing the current vector */
        lR[0] = trl_ddot(nrow, &W[j*ldw], one_i, &W[j*ldw], one_i );
        if (info->mpicom == -INT_MAX) {
            trl_dcopy(1, lR, one_i, &T[j*(m+1)+j], one_i);
        } else {
            MPI_Allreduce( lR, &T[j*(m+1)+j], 1, MPI_DOUBLE, MPI_SUM, info->mpicom );
        }
        T[j*(m+1)+j] = sqrt(T[j*(m+1)+j]);
        beta = one / T[j*(m+1)+j];
        trl_dscal( nrow, beta, &W[j*ldw], one_i );
    }
    #endif

    /* update R */
    for( j=1; j<=m; j++ ) {
        R[j*(m+1)+j-1] = T[(j-1)*(m+1)+j-1] * R[j*(m+1)+j-1] + T[j*(m+1)+j-1] * R[j*(m+1)+j];
    }
    for( j=0; j<=m; j++ ) {
        R[j*(m+1)+j] *= T[j*(m+1)+j];
    }
    add_clock_ticks(info, &(info->clk_qr), &(info->tick_q), clk1);
}


/*
// --------------------------------------------------------- //
// initialize/finalize Ca-TRLan                              //
// --------------------------------------------------------- //
*/
void catrlan_init( trl_info *info ) {
#ifdef CATRLan_SLA
  catrlan_initf_( &(info->blacs_ictxt), &(info->blacs_nprow), &(info->blacs_npcol),
                  &(info->blacs_myrow), &(info->blacs_mycol) );
#endif
}

void catrlan_clean( trl_info *info ) {
#ifdef CATRLan_SLA
  catrlan_cleanf_( &(info->blacs_ictxt) );
#endif
}


